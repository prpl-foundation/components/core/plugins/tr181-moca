/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "moca_priv.h"

#define ME "main"

static moca_t moca;

static int dm_mngr_load_controllers(void) {
    int rv = -1;
    amxd_dm_t* dm = moca_get_dm();
    amxd_object_t* moca_obj = amxd_dm_findf(dm, "MoCA");
    const amxc_var_t* controllers = amxd_object_get_param_value(moca_obj,
                                                                "SupportedControllers");
    amxc_var_t lcontrollers;
    amxm_shared_object_t* so = NULL;
    amxc_string_t mod_path;
    moca.mods_are_loaded = true;

    const char* mod_dir = GETP_CHAR(&moca.parser->config, "moca-manager.mod-dir");
    const char* external_mod_dir = GETP_CHAR(&moca.parser->config,
                                             "moca-manager.external-mod-dir");

    amxc_var_init(&lcontrollers);
    amxc_string_init(&mod_path, 0);

    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        amxc_string_setf(&mod_path, "%s/%s.so", external_mod_dir, name);
        SAH_TRACEZ_INFO(ME, "Loading controller '%s' (file = %s)", name, amxc_string_get(&mod_path, 0));
        rv = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        SAH_TRACEZ_INFO(ME, "Loading controller '%s' %s", name, rv ? "failed" : "successful");
        if(rv != 0) {
            moca.mods_are_loaded = false;
            amxc_string_setf(&mod_path, "%s/%s.so", mod_dir, DUMMY_MOCA_MOD_NAME);
            SAH_TRACEZ_INFO(ME, "Loading controller '%s' (file = %s)",
                            DUMMY_MOCA_MOD_NAME,
                            amxc_string_get(&mod_path, 0));
            rv = amxm_so_open(&so, DUMMY_MOCA_MOD_NAME, amxc_string_get(&mod_path, 0));
            when_failed_trace(rv, exit, ERROR, "Failed to load any MoCA module, moca manager will not be started");
            SAH_TRACEZ_ERROR(ME, "Failed to load MoCA modules, dummy module will be used");
            break;
        }
    }

exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lcontrollers);
    return rv;
}

static int moca_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    int rv = -1;
    SAH_TRACEZ_INFO(ME, "MoCA manager start");

    moca.moca_dm = dm;
    moca.parser = parser;
    moca.netdev_ctx = amxb_be_who_has("NetDev.");
    when_null_trace(moca.netdev_ctx, exit, ERROR, "failed to get bus context for NetDev");

    when_false_trace(netmodel_initialize(), exit, ERROR, "Failed to initialize libnetmodel");

    rv = dm_mngr_load_controllers();
    when_failed_trace(rv, exit, ERROR, "Failed to load MoCA modules");

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int moca_cleanup(UNUSED amxd_dm_t* dm, UNUSED amxo_parser_t* parser) {
    moca.moca_dm = NULL;
    moca.parser = NULL;
    moca.netdev_ctx = NULL;

    amxm_close_all();
    netmodel_cleanup();

    SAH_TRACEZ_INFO(ME, "MoCA manager stop");
    return 0;
}

amxd_dm_t* PRIVATE moca_get_dm(void) {
    return moca.moca_dm;
}

amxb_bus_ctx_t* PRIVATE moca_get_netdev_ctx(void) {
    return moca.netdev_ctx;
}

bool PRIVATE moca_mods_are_loaded(void) {
    return moca.mods_are_loaded;
}

amxo_parser_t* PRIVATE moca_get_parser(void) {
    return moca.parser;
}

amxc_var_t* moca_get_config(void) {
    return &(moca.parser->config);
}

int _moca_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "entry point %s, reason: %d", __func__, reason);

    switch(reason) {
    case AMXO_START:
        retval = moca_init(dm, parser);
        break;

    case AMXO_STOP:
        retval = moca_cleanup(dm, parser);
        break;
    }

    return retval;
}
