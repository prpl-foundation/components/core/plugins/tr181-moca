/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "moca_priv.h"
#include "moca_utils.h"
#include "moca_soc_utils.h"

#define ME "utils"

/**
 * @brief callback function for when the NetDev status changes
 */
static void _netdev_link_state_cb(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv) {
    SAH_TRACEZ_IN(ME);
    moca_info_t* moca = NULL;
    const char* new_state = NULL;
    const char* old_state = NULL;

    when_null_trace(priv, exit, ERROR, "private data should contain data");
    moca = (moca_info_t*) priv;

    old_state = GETP_CHAR(data, "parameters.State.from");
    new_state = GETP_CHAR(data, "parameters.State.to");

    SAH_TRACEZ_INFO(ME, "%s State changed from %s to %s", moca->object->name, old_state, new_state);
    dm_moca_update_status(moca, new_state);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
 * @brief Sets the Status parameter for the moca->object to the current State value in NetDev
 * @param moca pointer to moca info structure
 * @param ctx pointer to the bus context that provides NetDev.Link.
 */
static void netdev_link_get_initial_state(moca_info_t* moca, amxb_bus_ctx_t* ctx) {
    SAH_TRACEZ_IN(ME);
    int rv = AMXB_ERROR_UNKNOWN;
    amxc_var_t data;
    const char* new_state = NULL;
    amxc_string_t rel_path;

    amxc_string_init(&rel_path, 0);
    amxc_var_init(&data);

    when_null(moca, exit);

    if(ctx != NULL) {
        amxc_string_setf(&rel_path, "NetDev.Link.[Name==\"%s\"].State", moca->netdev_intf);
        rv = amxb_get(ctx, amxc_string_get(&rel_path, 0), 0, &data, 1);
        if(rv != AMXB_STATUS_OK) {
            SAH_TRACEZ_ERROR(ME, "NetDev link[%s] not found", moca->netdev_intf);
        } else {
            new_state = GETP_CHAR(&data, "0.0.State");
        }
    }
    if(new_state == NULL) {
        SAH_TRACEZ_ERROR(ME, "netdev status for %s could not be found", moca->netdev_intf);
        new_state = "Error";
    }

    dm_moca_update_status(moca, new_state);

exit:
    amxc_string_clean(&rel_path);
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
}


/**
 * @brief Changes the objects status in the datamodel
 * @param moca the pointer to moca info structure
 * @param status char* value containing the NetDev state
 * @return amxd_status_ok when the all actions are applied, otherwise an other error code and no changes in the data model are done.
 */
amxd_status_t dm_moca_update_status(moca_info_t* moca, const char* status) {
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxd_dm_t* dm = moca_get_dm();
    amxd_trans_t trans;
    const char* new_status = NULL;

    SAH_TRACEZ_INFO(ME, "Updating status: %s", status);

    amxd_trans_init(&trans);

    when_null_trace(moca, exit, ERROR, "moca interface info can not be NULL, can not change state");
    when_null_trace(moca->object, exit, ERROR, "object should not be NULL, can not change state");
    when_str_empty(status, exit);

    if(strcmp(status, "NotPresent") == 0) {
        new_status = "NotPresent";
    } else if(strcmp(status, "Down") == 0) {
        new_status = "Down";
    } else if(strcmp(status, "LowerLayerDown") == 0) {
        new_status = "LowerLayerDown";
    } else if(strcmp(status, "Dormant") == 0) {
        new_status = "Dormant";
    } else if(strcmp(status, "Up") == 0) {
        new_status = "Up";
    } else if(strcmp(status, "Error") == 0) {
        new_status = "Error";
    } else {
        new_status = "Unknown";
    }

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, moca->object);
    amxd_trans_set_value(cstring_t, &trans, "Status", new_status);
    rv = amxd_trans_apply(&trans, dm);

    moca->last_change = get_system_uptime();

    SAH_TRACEZ_INFO(ME, "New status: %s last_change:%d", new_status, moca->last_change);

exit:
    amxd_trans_clean(&trans);
    return rv;
}

/**
 * @brief Create a new subscription on NetDev that listens for changes to State
 * @param moca pointer to the moca info structure where to store the subscription
 */
void netdev_link_state_subscription_new(moca_info_t* moca) {
    SAH_TRACEZ_IN(ME);
    amxb_bus_ctx_t* ctx = NULL;
    amxc_string_t expr;

    amxc_string_init(&expr, 0);

    when_null(moca, exit);
    when_str_empty_trace(moca->netdev_intf, exit, ERROR, "not a valid netdev interface");

    ctx = moca_get_netdev_ctx();
    when_null_trace(ctx, exit, WARNING, "ctx not known, no subscription created");

    amxc_string_setf(&expr, "eobject == 'NetDev.Link.[%s].' && "
                     "contains('parameters.State')", moca->netdev_intf);
    if(moca->netdev_status_subscription != NULL) {
        amxb_subscription_delete(&(moca->netdev_status_subscription));
    }

    amxb_subscription_new(&(moca->netdev_status_subscription), ctx, "NetDev.Link.",
                          amxc_string_get(&expr, 0), _netdev_link_state_cb, moca);
    when_null_trace(moca->netdev_status_subscription, exit, WARNING, "failed to create subscription");
    netdev_link_get_initial_state(moca, ctx);

exit:
    amxc_string_clean(&expr);
    SAH_TRACEZ_OUT(ME);
    return;
}

static amxd_status_t add_rlapm_entry(amxd_object_t* mocaif_obj, uint32_t id) {
    amxd_status_t rv = amxd_status_ok;
    amxd_dm_t* dm = amxd_object_get_dm(mocaif_obj);
    amxd_object_t* rlapm_obj = amxd_object_get_child(mocaif_obj, "Rlapm");
    amxd_trans_t transaction;

    when_not_null(amxd_object_get_instance(rlapm_obj, NULL, id), skip);

    rv = amxd_trans_init(&transaction);
    when_failed_trace(rv, exit, ERROR, "failed to init a transaction object");
    rv = amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    when_failed_trace(rv, exit, ERROR, "failed to set the transaction attribute");

    rv = amxd_trans_select_object(&transaction, rlapm_obj);
    when_failed_trace(rv, exit, ERROR, "failed to select an object");
    rv = amxd_trans_add_inst(&transaction, id, NULL);
    when_failed_trace(rv, exit, ERROR, "failed to add instance to transaction");

    rv = amxd_trans_set_bool(&transaction, "Enable", true);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter Enable");
    rv = amxd_trans_set_uint32_t(&transaction, "Profile", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter Profile");
    rv = amxd_trans_set_uint32_t(&transaction, "Frequency", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter Frequency");
    rv = amxd_trans_set_uint32_t(&transaction, "GlobalAggrRxPwrLevel", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter GlobalAggrRxPwrLevel");
    rv = amxd_trans_set_uint32_t(&transaction, "PhyMargin", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter PhyMargin");
    rv = amxd_trans_set_cstring_t(&transaction, "Status", "notReady");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter Status");

    rv = amxd_trans_apply(&transaction, dm);
    when_failed_trace(rv, exit, ERROR, "failed to apply transaction");

    SAH_TRACEZ_INFO(ME, "Added rlapm instance %d on interface %s", id, amxd_object_get_name(mocaif_obj, AMXD_OBJECT_NAMED));

exit:
    amxd_trans_clean(&transaction);
skip:
    return rv;
}

static amxd_status_t add_sapm_entry(amxd_object_t* mocaif_obj, uint32_t id) {
    amxd_status_t rv = amxd_status_ok;
    amxd_dm_t* dm = amxd_object_get_dm(mocaif_obj);
    amxd_object_t* sapm_obj = amxd_object_get_child(mocaif_obj, "Sapm");
    amxd_trans_t transaction;

    when_not_null(amxd_object_get_instance(sapm_obj, NULL, id), skip);

    rv = amxd_trans_init(&transaction);
    when_failed_trace(rv, exit, ERROR, "failed to init a transaction object");
    rv = amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    when_failed_trace(rv, exit, ERROR, "failed to set the transaction attribute");

    rv = amxd_trans_select_object(&transaction, sapm_obj);
    when_failed_trace(rv, exit, ERROR, "failed to select an object");
    rv = amxd_trans_add_inst(&transaction, id, NULL);
    when_failed_trace(rv, exit, ERROR, "failed to add instance to transaction");

    rv = amxd_trans_set_bool(&transaction, "Enable", true);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter Enable");
    rv = amxd_trans_set_uint32_t(&transaction, "Profile", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter Profile");
    rv = amxd_trans_set_uint32_t(&transaction, "Frequency", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter Frequency");
    rv = amxd_trans_set_uint32_t(&transaction, "AggrRxPwrLevelThreshold", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter AggrRxPwrLevelThreshold");
    rv = amxd_trans_set_cstring_t(&transaction, "PhyMargin", "");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter PhyMargin");
    rv = amxd_trans_set_cstring_t(&transaction, "Status", "notReady");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter Status");

    rv = amxd_trans_apply(&transaction, dm);
    when_failed_trace(rv, exit, ERROR, "failed to apply transaction");

    SAH_TRACEZ_INFO(ME, "Added sapm instance %d on interface %s", id, amxd_object_get_name(mocaif_obj, AMXD_OBJECT_NAMED));

exit:
    amxd_trans_clean(&transaction);
skip:
    return rv;
}

int moca_interface_create(moca_info_t* moca, amxc_var_t* if_data, amxc_var_t* ret) {
    uint32_t num_sapm_entries, num_rlapm_entries;
    int rc = -1;
    amxc_var_t in_data;
    amxc_var_t ret_data;

    when_null_trace(moca, exit, ERROR, "Object moca is null");
    SAH_TRACEZ_INFO(ME, "Executing function for name: %s netdev_name: %s", moca->name_intf, moca->netdev_intf);

    when_failed_trace(mod_moca_execute_function(moca->object, "create-moca-iface", if_data, ret),
                      exit, ERROR, "create-moca-iface failed");

    when_failed_trace(amxc_var_init(&in_data), exit, ERROR, "Failed to init in_data");
    when_failed_trace(amxc_var_init(&ret_data), exit, ERROR, "Failed to init ret_data");

    amxc_var_set_type(&in_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &in_data, "InternalName", moca->name_intf);

    when_failed_trace(mod_moca_execute_function(moca->object, "retrieve-rlapm-number", &in_data, &ret_data),
                      clean, ERROR, "retrieve-rlapm-number failed");

    num_rlapm_entries = GETP_UINT32(&ret_data, "RlapmNumberOfEntries");

    SAH_TRACEZ_NOTICE(ME, "%s, RlapmNumberOfEntries=%d", moca->name_intf, num_rlapm_entries);

    for(uint32_t i = 0; i < num_rlapm_entries; i++) {
        when_failed_trace(add_rlapm_entry(moca->object, i + 1), clean, ERROR,
                          "Could not create rlapm instance %d on %s", i, moca->name_intf);
    }

    when_failed_trace(mod_moca_execute_function(moca->object, "retrieve-sapm-number", &in_data, &ret_data),
                      clean, ERROR, "retrieve-sapm-number failed");

    num_sapm_entries = GETP_UINT32(&ret_data, "SapmNumberOfEntries");

    SAH_TRACEZ_NOTICE(ME, "%s, SapmNumberOfEntries=%d", moca->name_intf, num_sapm_entries);

    for(uint32_t i = 0; i < num_sapm_entries; i++) {
        when_failed_trace(add_sapm_entry(moca->object, i + 1), clean, ERROR,
                          "Could not create sapm instance %d on %s", i, moca->name_intf);
    }

    rc = 0;

clean:
    amxc_var_clean(&in_data);
    amxc_var_clean(&ret_data);
exit:
    return rc;
}

/**
 * @brief Initialize a new moca info structure
 * @param moca pointer to moca info structure
 * @param mocaif_obj pointer to moca object
 * @param name_intf internal interface name
 * @param netdev_intf corresponding NetDev interface name
 */
amxd_status_t moca_info_new(moca_info_t** moca, amxd_object_t* mocaif_obj, const char* name_intf, const char* netdev_intf, bool hwpresent) {
    amxd_status_t rv = amxd_status_invalid_function_argument;

    when_null_trace(moca, exit, ERROR, "moca_info can not be null, invalid argument");
    when_str_empty(name_intf, exit);
    when_str_empty(netdev_intf, exit);

    SAH_TRACEZ_INFO(ME, "allocating new moca info for name: %s netdev_name: %s", name_intf, netdev_intf);

    *moca = (moca_info_t*) calloc(1, sizeof(moca_info_t));
    when_null_status(*moca, exit, rv = amxd_status_out_of_mem);

    (*moca)->last_change = get_system_uptime();
    (*moca)->present = hwpresent;
    (*moca)->name_intf = (char*) calloc(1, 65);
    when_null_status((*moca)->name_intf, clean, rv = amxd_status_out_of_mem);
    strncpy((*moca)->name_intf, name_intf, 64);
    (*moca)->netdev_intf = (char*) calloc(1, 65);
    when_null_status((*moca)->netdev_intf, clean, rv = amxd_status_out_of_mem);
    strncpy((*moca)->netdev_intf, netdev_intf, 64);
    (*moca)->object = mocaif_obj;

    rv = amxd_status_ok;

clean:
    if(rv != amxd_status_ok) {
        free((*moca)->netdev_intf);
        free((*moca)->name_intf);
        free(*moca);
        *moca = NULL;
    }
exit:
    return rv;
}

/**
 * @brief Cleanup a moca info structure
 * @param moca pointer to the moca info structure
 */
amxd_status_t moca_info_clean(moca_info_t** moca) {
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxc_var_t data;
    amxc_var_t ret;

    when_null_trace(moca, exit, ERROR, "Passed argument is NULL");
    when_null_trace(*moca, exit, ERROR, "moca_info can not be null, invalid argument");

    when_failed_trace(amxc_var_init(&data), clean, ERROR, "Failed to init data");
    when_failed_trace(amxc_var_init(&ret), clean, ERROR, "Failed to init ret");

    SAH_TRACEZ_INFO(ME, "freeing moca info for name: %s", (*moca)->name_intf);

    if((*moca)->present) {
        amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &data, "InternalName", (*moca)->name_intf);

        when_failed_trace(mod_moca_execute_function((*moca)->object, "destroy-moca-iface", &data, &ret),
                          amxvar_clean, ERROR, "destroy-moca-iface failed");
    }

    rv = amxd_status_ok;

amxvar_clean:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
clean:
    free((*moca)->netdev_intf);
    free((*moca)->name_intf);
    amxb_subscription_delete(&(*moca)->netdev_status_subscription);
    free(*moca);
    *moca = NULL;
exit:
    return rv;
}

/**
 * @brief Enable or disable a moca interface
 * @param moca pointer to the moca info structure
 * @param enable Boolean to indicate whether the interface should be
 *               enabled or disabled
 * @return 0 in case of success, non 0 otherwise
 */
int moca_interface_set_enabled(moca_info_t* moca, bool enable) {
    int ret = -1;
    when_null_trace(moca, exit, ERROR, "interface can not be null, invalid argument");

    ret = set_moca_enabled(moca, enable);

exit:
    return ret;
}

uint32_t get_system_uptime(void) {
    uint32_t uptime = 0;
    struct sysinfo info;
    when_failed_trace(sysinfo(&info), exit, ERROR, "Could not read total uptime of the system");
    uptime = info.uptime;
exit:
    return uptime;
}
