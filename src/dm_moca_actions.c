/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "moca_priv.h"
#include "moca_actions.h"
#include "moca_utils.h"

#define ME "actions"

amxd_status_t _check_is_empty_or_in(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    amxd_status_t rv = amxd_status_unknown_error;

    rv = amxd_action_param_check_is_in(object, param, reason, args, retval, priv);

    if(rv != amxd_status_ok) {
        char* input = amxc_var_dyncast(cstring_t, args);
        if((input == NULL) || (*input == 0)) {
            rv = amxd_status_ok;
        }
        free(input);
    }

    return rv;
}

typedef amxd_object_t* (* moca_info_fetch_t)(amxd_object_t* object);
typedef amxd_object_t* (* moca_info_push_t)(amxd_object_t* object, amxd_param_t* const param, const amxc_var_t* const args);

static amxd_status_t moca_info_write(amxd_object_t* const object,
                                     amxd_param_t* const param,
                                     amxd_action_t reason,
                                     moca_info_push_t moca_push,
                                     const amxc_var_t* const args,
                                     amxc_var_t* const retval,
                                     void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    SAH_TRACEZ_INFO(ME, "%s.%s reason: %d ", amxd_object_get_name(object, AMXD_OBJECT_NAMED), param->name, reason);

    if(reason != action_param_write) {
        SAH_TRACEZ_NOTICE(ME, "wrong reason, expected action_param_write(%d) got %d", action_param_write, reason);
        status = amxd_status_invalid_action;
        goto exit;
    }

    amxd_object_t* moca_info = (*moca_push)(object, param, args);

    status = amxd_action_object_write(moca_info,
                                      param,
                                      reason,
                                      args,
                                      retval,
                                      priv);

exit:
    return status;
}

static amxd_status_t moca_info_read(amxd_object_t* const object,
                                    amxd_param_t* const param,
                                    amxd_action_t reason,
                                    moca_info_fetch_t moca_fetch,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    SAH_TRACEZ_INFO(ME, "%s reason: %d ", amxd_object_get_name(object, AMXD_OBJECT_NAMED), reason);

    if(reason != action_object_read) {
        SAH_TRACEZ_NOTICE(ME, "wrong reason, expected action_object_read(%d) got %d", action_object_read, reason);
        status = amxd_status_invalid_action;
        goto exit;
    }

    amxd_object_t* moca_info = (*moca_fetch)(object);

    status = amxd_action_object_read(moca_info,
                                     param,
                                     reason,
                                     args,
                                     retval,
                                     priv);

exit:
    return status;
}

amxd_status_t _cfg_info_write(amxd_object_t* const object,
                              amxd_param_t* const param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    return moca_info_write(object, param, reason, moca_cfg_info_push, args, retval, priv);
}

amxd_status_t _cfg_info_read(amxd_object_t* const object,
                             amxd_param_t* const param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv) {
    return moca_info_read(object, param, reason, moca_cfg_info_fetch, args, retval, priv);
}

amxd_status_t _static_info_read(amxd_object_t* const object,
                                amxd_param_t* const param,
                                amxd_action_t reason,
                                const amxc_var_t* const args,
                                amxc_var_t* const retval,
                                void* priv) {
    return moca_info_read(object, param, reason, moca_static_info_fetch, args, retval, priv);
}

amxd_status_t _dyn_info_read(amxd_object_t* const object,
                             amxd_param_t* const param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv) {
    return moca_info_read(object, param, reason, moca_dyn_info_fetch, args, retval, priv);
}

amxd_status_t _rlapm_info_read(amxd_object_t* const object,
                               amxd_param_t* const param,
                               amxd_action_t reason,
                               const amxc_var_t* const args,
                               amxc_var_t* const retval,
                               void* priv) {
    return moca_info_read(object, param, reason, moca_rlapm_info_fetch, args, retval, priv);
}

amxd_status_t _sapm_info_read(amxd_object_t* const object,
                              amxd_param_t* const param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    return moca_info_read(object, param, reason, moca_sapm_info_fetch, args, retval, priv);
}

amxd_status_t _qos_info_read(amxd_object_t* const object,
                             amxd_param_t* const param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv) {
    return moca_info_read(object, param, reason, moca_qos_info_fetch, args, retval, priv);
}

amxd_status_t _flow_info_read(amxd_object_t* const object,
                              amxd_param_t* const param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    return moca_info_read(object, param, reason, moca_flow_info_fetch, args, retval, priv);
}

amxd_status_t _node_info_read(amxd_object_t* const object,
                              amxd_param_t* const param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    return moca_info_read(object, param, reason, moca_node_info_fetch, args, retval, priv);
}

amxd_status_t _mesh_info_read(amxd_object_t* const object,
                              amxd_param_t* const param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    return moca_info_read(object, param, reason, moca_mesh_info_fetch, args, retval, priv);
}

amxd_status_t _bridge_info_read(amxd_object_t* const object,
                                amxd_param_t* const param,
                                amxd_action_t reason,
                                const amxc_var_t* const args,
                                amxc_var_t* const retval,
                                void* priv) {
    return moca_info_read(object, param, reason, moca_bridge_info_fetch, args, retval, priv);
}

amxd_status_t _lastchange_on_read(amxd_object_t* const object,
                                  UNUSED amxd_param_t* const param,
                                  amxd_action_t reason,
                                  UNUSED const amxc_var_t* const args,
                                  amxc_var_t* const retval,
                                  UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    moca_info_t* moca = NULL;
    uint32_t since_change = 0;
    uint32_t uptime = 0;

    if(reason != action_param_read) {
        SAH_TRACEZ_NOTICE(ME, "wrong reason, expected action_param_read(%d) got %d", action_param_read, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, skip, ERROR, "object can not be NULL");
    // when a parent object of port gets read,
    // this function will also be called for the template object
    when_null(object->priv, skip);
    moca = (moca_info_t*) object->priv;
    uptime = get_system_uptime();
    rv = (uptime > (uint32_t) moca->last_change) ? amxd_status_ok : amxd_status_invalid_value;
    when_failed_trace(rv, skip, ERROR, "UpTime=%d is smaller than last change=%d", uptime, moca->last_change);
    since_change = uptime - moca->last_change;

skip:
    when_failed_trace(amxc_var_set_uint32_t(retval, since_change),
                      exit, ERROR, "failed to set parameter lastchange");
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _moca_instance_removed(amxd_object_t* object,
                                     UNUSED amxd_param_t* param,
                                     amxd_action_t reason,
                                     UNUSED const amxc_var_t* const args,
                                     UNUSED amxc_var_t* const retval,
                                     UNUSED void* priv) {
    moca_info_t* moca = NULL;
    amxd_status_t rv = amxd_status_invalid_function_argument;

    if(reason != action_object_destroy) {
        SAH_TRACEZ_NOTICE(ME, "Wrong reason, expected action_object_destroy(%d) got %d", action_object_destroy, reason);
        rv = amxd_status_invalid_action;
        goto exit;
    }

    when_null_trace(object, exit, ERROR, "object can not be null, invalid argument");

    SAH_TRACEZ_INFO(ME, "Removing: %s", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    if(object->type != amxd_object_instance) {
        SAH_TRACEZ_INFO(ME, "Skipping template object, only remove instances");
        rv = amxd_status_ok;
        goto exit;
    }

    when_null(object->priv, exit);
    moca = (moca_info_t*) object->priv;

    rv = moca_info_clean(&moca);

exit:
    return rv;
}
