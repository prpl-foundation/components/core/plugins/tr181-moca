/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "moca_priv.h"
#include "moca_actions.h"
#include "moca_utils.h"

#include "moca_soc_utils.h"

#define ME "utils"

/**
 * Definition for the configuration parameters
 */
static const char* CfgParams[] = {
    "Enable",
    "PreferredNC",
    "PrivacyEnabledSetting",
    "TpcTargetRateNper",
    "FreqCurrentMaskSetting",
    "KeyPassphrase",
    "TxPowerLimit",
    "PowerCntlPhyTarget",
    "NodeTabooMask",
    NULL
};

/**
 * Definition for the static parameters
 */
static const char* StaticParams[] = {
    "InternalName",
    "MACAddress",
    "FirmwareVersion",
    "SupportedBands",
    "MaxBitRate",
    "AggregationSize",
    "AeNumber",
    "SupportedIngressPqosFlows",
    "SupportedEgressPqosFlows",
    "HighestVersion",
    "FreqCapabilityMask",
    "NetworkTabooMask",
    "TxBcastPowerReduction",
    "QAM256Capable",
    "AvbSupport",
    "PacketAggregationCapability",
    NULL
};

/**
 * Definition for the dynamic parameters
 */
static const char* DynamicParams[] = {
    "Status",
    "PasswordHash",
    "PowerStateCap",
    "MaxIngressBW",
    "MaxEgressBW",
    "CurrentVersion",
    "NetworkCoordinator",
    "NodeID",
    "BackupNC",
    "NumNodes",
    "PrivacyEnabled",
    "FreqCurrentMask",
    "CurrentOperFreq",
    "LastOperFreq",
    "TxBcastRate",
    "ResetCount",
    "LinkDownCount",
    "LmoNodeID",
    "NetworkState",
    "PrimaryChannelOffset",
    "SecondaryChannelOffset",
    "ResetReason",
    "NcVersion",
    "LinkState",
    NULL
};

/**
 * Definition for the Rlapm parameters
 */
static const char* RlapmParams[] = {
    "Enable",
    "Profile",
    "Frequency",
    "GlobalAggrRxPwrLevel",
    "PhyMargin",
    "Status",
    NULL
};

/**
 * Definition for the Sapm parameters
 */
static const char* SapmParams[] = {
    "Enable",
    "Profile",
    "Frequency",
    "AggrRxPwrLevelThreshold",
    "PhyMargin",
    "Status",
    NULL
};

/**
 * Definition for the QoS parameters
 */
static const char* QoSParams[] = {
    "EgressNumFlows",
    "IngressNumFlows",
    NULL
};

/**
 * Definition for the FlowStats parameters
 */
static const char* FlowParams[] = {
    "FlowID",
    "PacketDA",
    "MaxRate",
    "MaxBurstSize",
    "LeaseTime",
    "Tag",
    "LeaseTimeLeft",
    "FlowPackets",
    "IngressGuid",
    "EgressGuid",
    "MaximumLatency",
    "ShortTermAvgRatio",
    "FlowPer",
    "IngressClassify",
    "VlanTag",
    "DscpMoca",
    "Dfid",
    NULL
};

/**
 * Definition for the Node parameters
 */
static const char* nodeParams[] = {
    "MACAddress",
    "NodeID",
    "PreferredNC",
    "HighestVersion",
    "PHYTxRate",
    "PHYRxRate",
    "TxPowerControlReduction",
    "RxPowerLevel",
    "TxBcastRate",
    "RxBcastPowerLevel",
    "TxPackets",
    "TxDrops",
    "RxPackets",
    "RxCorrected",
    "RxErroredAndMissedPackets",
    "BondingCapable",
    "QAM256Capable",
    "PacketAggregationCapability",
    "RxSNR",
    "Active",
    "SupportedIngressPqosFlows",
    "SupportedEgressPqosFlows",
    "AggregationSize",
    "AeNumber",
    "PowerState",
    "PowerStateCapability",
    "Moca25PhyCapable",
    NULL
};

/**
 * Definition for the mesh parameters
 */
static const char* meshParams[] = {
    "TxNodeIndex",
    "RxNodeIndex",
    "TxRate",
    "TxRateNper",
    "TxRateVlper",
    NULL
};

/**
 * Definition for the bridge parameters
 */
static const char* bridgeParams[] = {
    "MACAddresses",
    "NodeIndex",
    NULL
};

static const char* get_moca_ctrl(amxd_object_t* mocaif_obj) {
    const amxc_var_t* moca_ctrl = amxd_object_get_param_value(mocaif_obj, "MoCAController");

    const char* moca_ctrl_name = NULL;

    if(moca_mods_are_loaded()) {
        moca_ctrl_name = GET_CHAR(moca_ctrl, NULL);
    } else {
        moca_ctrl_name = DUMMY_MOCA_MOD_NAME;
    }

    return moca_ctrl_name;
}


int mod_moca_execute_function(amxd_object_t* mocaif_obj, const char* function, amxc_var_t* data, amxc_var_t* ret) {
    const char* moca_ctrl = NULL;
    int rv = -1;

    when_str_empty(function, exit);
    when_null_trace(mocaif_obj, exit, ERROR, "can not execute %s, no object given", function);

    moca_ctrl = get_moca_ctrl(mocaif_obj);
    SAH_TRACEZ_INFO(ME, "%s -> call implementation (controller = '%s')", function, moca_ctrl);
    rv = amxm_execute_function(moca_ctrl, MOD_MOCA_CTRL, function, data, ret);
    when_failed_trace(rv, exit, ERROR, "amxm_execute_function failed");

exit:
    return rv;
}

static void moca_info_fill(amxd_object_t* mocaif_obj, const char* params[], amxc_var_t* moca_info) {
    amxd_param_t* param = NULL;
    uint32_t type = 0;
    int status = 0;

    SAH_TRACEZ_INFO(ME, "moca_info is %s null", amxc_var_is_null(moca_info) ? "" : "not");
    for(int i = 0; params[i] != NULL; i++) {
        param = amxd_object_get_param_def(mocaif_obj, params[i]);
        type = amxc_var_type_of(&param->value);
        SAH_TRACEZ_INFO(ME, "Filling parameter %s, type=%d", params[i], type);

        if(!amxc_var_is_null(moca_info)) {
            status = amxc_var_convert(&param->value, GET_ARG(moca_info, params[i]), type);
            when_failed_trace(status, exit, ERROR, "failed to set parameter %s", params[i]);
        }
    }

exit:
    return;
}

static amxd_object_t* moca_iface_info_fetch(amxd_object_t* object, const char* params[], const char* function) {
    amxc_var_t data;
    amxc_var_t ret;
    moca_info_t* moca = NULL;

    when_null_trace(object, exit, ERROR, "object is NULL");
    when_failed_trace(amxc_var_init(&ret), exit, ERROR, "Failed to init ret");
    when_failed_trace(amxc_var_init(&data), exit, ERROR, "Failed to init data");

    moca = (moca_info_t*) object->priv;
    when_null_trace(moca, exit, INFO, "moca info is still NULL");
    when_null_trace(moca->name_intf, exit, ERROR, "Internal interface name must be set");

    SAH_TRACEZ_INFO(ME, "%s, fetching via %s data", amxd_object_get_name(object, AMXD_OBJECT_NAMED), function);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "InternalName", moca->name_intf);
    if(moca->present) {
        when_failed_trace(mod_moca_execute_function(object, function, &data, &ret),
                          exit, ERROR, "%s failed", function);
    }
    moca_info_fill(object, params, &ret);

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return object;
}

static amxd_object_t* moca_iface_subtable_info_fetch(amxd_object_t* object, const char* params[], const char* function) {
    amxc_var_t data;
    amxc_var_t ret;
    amxd_object_t* mocaif_obj;
    moca_info_t* moca = NULL;

    when_failed_trace(amxc_var_init(&ret), exit, ERROR, "Failed to init ret");
    when_failed_trace(amxc_var_init(&data), exit, ERROR, "Failed to init data");
    when_null_trace(object, exit, ERROR, "object is NULL");
    mocaif_obj = amxd_object_get_parent(amxd_object_get_parent(object));

    SAH_TRACEZ_INFO(ME, "%s, fetching %s for %d data", amxd_object_get_name(mocaif_obj, AMXD_OBJECT_NAMED), function, amxd_object_get_index(object));

    moca = (moca_info_t*) mocaif_obj->priv;
    when_null_trace(moca, exit, INFO, "moca info is still NULL");
    when_null_trace(moca->name_intf, exit, ERROR, "Internal interface name must be set");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "InternalName", moca->name_intf);
    amxc_var_add_key(uint32_t, &data, "Id", amxd_object_get_index(object));
    if(moca->present) {
        when_failed_trace(mod_moca_execute_function(mocaif_obj, function, &data, &ret),
                          exit, ERROR, "%s failed", function);
    }
    moca_info_fill(object, params, &ret);

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return object;
}

amxd_object_t* moca_cfg_info_fetch(amxd_object_t* object) {
    return moca_iface_info_fetch(object, CfgParams, "retrieve-cfg-info");
}

amxd_object_t* moca_dyn_info_fetch(amxd_object_t* object) {
    return moca_iface_info_fetch(object, DynamicParams, "retrieve-dyn-info");
}

amxd_object_t* moca_static_info_fetch(amxd_object_t* object) {
    return moca_iface_info_fetch(object, StaticParams, "retrieve-static-info");
}

amxd_object_t* moca_rlapm_info_fetch(amxd_object_t* object) {
    return moca_iface_subtable_info_fetch(object, RlapmParams, "retrieve-rlapm-info");
}

amxd_object_t* moca_sapm_info_fetch(amxd_object_t* object) {
    return moca_iface_subtable_info_fetch(object, SapmParams, "retrieve-sapm-info");
}

amxd_object_t* moca_qos_info_fetch(amxd_object_t* object) {
    amxc_var_t data;
    amxc_var_t ret;
    moca_info_t* moca = NULL;

    when_failed_trace(amxc_var_init(&ret), exit, ERROR, "Failed to init ret");
    when_failed_trace(amxc_var_init(&data), exit, ERROR, "Failed to init data");
    when_null_trace(object, exit, ERROR, "object is NULL");
    amxd_object_t* moca_if = amxd_object_get_parent(object);
    moca = (moca_info_t*) moca_if->priv;
    when_null_trace(moca, exit, INFO, "moca info is still NULL");
    when_null_trace(moca->name_intf, exit, ERROR, "Internal interface name must be set");

    SAH_TRACEZ_INFO(ME, "%s, fetching qos data", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "InternalName", moca->name_intf);
    if(moca->present) {
        when_failed_trace(mod_moca_execute_function(moca_if, "retrieve-qos-info", &data, &ret),
                          exit, ERROR, "retrieve-qos-info failed");
    }
    moca_info_fill(object, QoSParams, &ret);

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return object;
}

amxd_object_t* moca_flow_info_fetch(amxd_object_t* object) {
    amxc_var_t data;
    amxc_var_t ret;
    amxd_object_t* mocaif_obj;
    moca_info_t* moca = NULL;

    when_failed_trace(amxc_var_init(&ret), exit, ERROR, "Failed to init ret");
    when_failed_trace(amxc_var_init(&data), exit, ERROR, "Failed to init data");
    when_null_trace(object, exit, ERROR, "object is NULL");
    mocaif_obj = amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(object)));

    SAH_TRACEZ_INFO(ME, "%s, fetching flow %d data", amxd_object_get_name(object, AMXD_OBJECT_NAMED), amxd_object_get_index(object));

    moca = (moca_info_t*) mocaif_obj->priv;
    when_null_trace(moca, exit, INFO, "moca info is still NULL");
    when_null_trace(moca->name_intf, exit, ERROR, "Internal interface name must be set");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "InternalName", moca->name_intf);
    amxc_var_add_key(uint32_t, &data, "Id", amxd_object_get_index(object));
    if(moca->present) {
        when_failed_trace(mod_moca_execute_function(mocaif_obj, "retrieve-flow-info", &data, &ret),
                          exit, ERROR, "retrieve-flow-info failed");
    }
    moca_info_fill(object, FlowParams, &ret);

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return object;
}

amxd_object_t* moca_node_info_fetch(amxd_object_t* object) {
    return moca_iface_subtable_info_fetch(object, nodeParams, "retrieve-node-info");
}

amxd_object_t* moca_mesh_info_fetch(amxd_object_t* object) {
    return moca_iface_subtable_info_fetch(object, meshParams, "retrieve-mesh-info");
}

amxd_object_t* moca_bridge_info_fetch(amxd_object_t* object) {
    return moca_iface_subtable_info_fetch(object, bridgeParams, "retrieve-bridge-info");
}

amxd_object_t* moca_cfg_info_push(amxd_object_t* object, amxd_param_t* const param, const amxc_var_t* const new_value) {
    amxc_var_t data;
    amxc_var_t ret;
    moca_info_t* moca = NULL;

    when_null_trace(object, exit, ERROR, "object is NULL");
    when_failed_trace(amxc_var_init(&ret), exit, ERROR, "Failed to init ret");
    when_failed_trace(amxc_var_init(&data), exit, ERROR, "Failed to init data");

    moca = (moca_info_t*) object->priv;
    when_null_trace(moca, exit, INFO, "moca info is still NULL");
    when_null_trace(moca->name_intf, exit, ERROR, "Internal interface name must be set");
    when_null_trace(param, exit, ERROR, "Passed param argument is NULL");
    when_null_trace(new_value, exit, ERROR, "Passed new_value argument is NULL");

    SAH_TRACEZ_INFO(ME, "%s, Setting %s", amxd_object_get_name(object, AMXD_OBJECT_NAMED), param->name);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "InternalName", moca->name_intf);

    if(!strcmp(param->name, "Enable")) {
        amxc_var_add_key(bool, &data, "Enable", amxc_var_dyncast(bool, new_value));
    } else if(!strcmp(param->name, "PreferredNC")) {
        amxc_var_add_key(bool, &data, "PreferredNC", amxc_var_dyncast(bool, new_value));
    } else if(!strcmp(param->name, "PrivacyEnabledSetting")) {
        amxc_var_add_key(bool, &data, "PrivacyEnabledSetting", amxc_var_dyncast(bool, new_value));
    } else if(!strcmp(param->name, "TpcTargetRateNper")) {
        amxc_var_add_key(uint32_t, &data, "TpcTargetRateNper", amxc_var_dyncast(uint32_t, new_value));
    } else if(!strcmp(param->name, "FreqCurrentMaskSetting")) {
        amxc_var_add_key(cstring_t, &data, "FreqCurrentMaskSetting", amxc_var_dyncast(cstring_t, new_value));
    } else if(!strcmp(param->name, "KeyPassphrase")) {
        amxc_var_add_key(cstring_t, &data, "KeyPassphrase", amxc_var_dyncast(cstring_t, new_value));
    } else if(!strcmp(param->name, "TxPowerLimit")) {
        amxc_var_add_key(uint32_t, &data, "TxPowerLimit", amxc_var_dyncast(uint32_t, new_value));
    } else if(!strcmp(param->name, "PowerCntlPhyTarget")) {
        amxc_var_add_key(uint32_t, &data, "PowerCntlPhyTarget", amxc_var_dyncast(uint32_t, new_value));
    } else if(!strcmp(param->name, "NodeTabooMask")) {
        amxc_var_add_key(cstring_t, &data, "NodeTabooMask", amxc_var_dyncast(cstring_t, new_value));
    }

    if(moca->present) {
        when_failed_trace(mod_moca_execute_function(object, "set-cfg-info", &data, &ret),
                          exit, ERROR, "set-cfg-info failed");
    }
exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return object;
}

int set_moca_enabled(moca_info_t* moca, bool enable) {
    int rv = -1;
    amxc_var_t data;
    amxc_var_t ret;

    when_failed_trace(amxc_var_init(&ret), exit, ERROR, "Failed to init ret");
    when_failed_trace(amxc_var_init(&data), exit, ERROR, "Failed to init data");

    when_null_trace(moca, exit, ERROR, "Passed moca info is NULL");
    when_null_trace(moca->object, exit, ERROR, "moca object is NULL");
    when_null_trace(moca->name_intf, exit, ERROR, "Internal interface name must be set");

    SAH_TRACEZ_INFO(ME, "%s, Setting %s", amxd_object_get_name(moca->object, AMXD_OBJECT_NAMED), enable ? "enabled" : "disabled");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "InternalName", moca->name_intf);
    amxc_var_add_key(bool, &data, "Enable", enable);
    if(moca->present) {
        when_failed_trace(mod_moca_execute_function(moca->object, "set-cfg-info", &data, &ret),
                          exit, ERROR, "set-cfg-info failed");
    }

    rv = 0;

exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return rv;
}

