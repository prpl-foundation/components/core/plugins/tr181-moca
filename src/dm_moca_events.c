/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "moca_priv.h"
#include "moca_events.h"
#include "moca_utils.h"
#include "moca_soc_utils.h"

#define ME "events"

static amxd_status_t add_flow_entry(amxd_object_t* mocaif_obj, uint32_t id) {
    amxd_status_t rv;
    amxd_dm_t* dm = amxd_object_get_dm(mocaif_obj);
    amxd_object_t* flows_obj = amxd_object_get_child(amxd_object_get_child(mocaif_obj, "QoS"), "FlowStats");
    amxd_trans_t transaction;

    rv = amxd_trans_init(&transaction);
    when_failed_trace(rv, exit, ERROR, "failed to init a transaction object");
    rv = amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    when_failed_trace(rv, exit, ERROR, "failed to set the transaction attribute");

    rv = amxd_trans_select_object(&transaction, flows_obj);
    when_failed_trace(rv, exit, ERROR, "failed to select an object");
    rv = amxd_trans_add_inst(&transaction, id, NULL);
    when_failed_trace(rv, exit, ERROR, "failed to add instance to transaction");

    rv = amxd_trans_set_cstring_t(&transaction, "FlowID", "");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter FlowID");
    rv = amxd_trans_set_cstring_t(&transaction, "PacketDA", "");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter PacketDA");
    rv = amxd_trans_set_uint32_t(&transaction, "MaxRate", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter MaxRate");
    rv = amxd_trans_set_uint32_t(&transaction, "MaxBurstSize", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter MaxBurstSize");
    rv = amxd_trans_set_uint32_t(&transaction, "LeaseTime", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter LeaseTime");
    rv = amxd_trans_set_uint32_t(&transaction, "Tag", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter Tag");
    rv = amxd_trans_set_uint32_t(&transaction, "LeaseTimeLeft", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter LeaseTimeLeft");
    rv = amxd_trans_set_uint32_t(&transaction, "FlowPackets", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter FlowPackets");
    rv = amxd_trans_set_cstring_t(&transaction, "IngressGuid", "");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter IngressGuid");
    rv = amxd_trans_set_cstring_t(&transaction, "EgressGuid", "");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter EgressGuid");
    rv = amxd_trans_set_uint32_t(&transaction, "MaximumLatency", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter MaximumLatency");
    rv = amxd_trans_set_uint32_t(&transaction, "ShortTermAvgRatio", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter ShortTermAvgRatio");
    rv = amxd_trans_set_uint32_t(&transaction, "FlowPer", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter FlowPer");
    rv = amxd_trans_set_cstring_t(&transaction, "IngressClassify", "");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter IngressClassify");
    rv = amxd_trans_set_uint32_t(&transaction, "VlanTag", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter VlanTag");
    rv = amxd_trans_set_uint32_t(&transaction, "DscpMoca", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter DscpMoca");
    rv = amxd_trans_set_uint32_t(&transaction, "Dfid", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter Dfid");

    rv = amxd_trans_apply(&transaction, dm);
    when_failed_trace(rv, exit, ERROR, "failed to apply transaction");

    SAH_TRACEZ_INFO(ME, "Added flow instance %d on interface %s", id, amxd_object_get_name(mocaif_obj, AMXD_OBJECT_NAMED));

exit:
    amxd_trans_clean(&transaction);
    return rv;
}

static amxd_status_t add_node_entry(amxd_object_t* mocaif_obj, uint32_t id) {
    amxd_status_t rv;
    amxd_dm_t* dm = amxd_object_get_dm(mocaif_obj);
    amxd_object_t* nodes_obj = amxd_object_get_child(mocaif_obj, "AssociatedDevice");
    amxd_trans_t transaction;

    rv = amxd_trans_init(&transaction);
    when_failed_trace(rv, exit, ERROR, "failed to init a transaction object");
    rv = amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    when_failed_trace(rv, exit, ERROR, "failed to set the transaction attribute");

    rv = amxd_trans_select_object(&transaction, nodes_obj);
    when_failed_trace(rv, exit, ERROR, "failed to select an object");
    rv = amxd_trans_add_inst(&transaction, id, NULL);
    when_failed_trace(rv, exit, ERROR, "failed to add instance to transaction");

    rv = amxd_trans_set_cstring_t(&transaction, "MACAddress", "");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter MACAddress");
    rv = amxd_trans_set_uint32_t(&transaction, "NodeID", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter NodeID");
    rv = amxd_trans_set_bool(&transaction, "PreferredNC", false);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter PreferredNC");
    rv = amxd_trans_set_cstring_t(&transaction, "HighestVersion", "");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter HighestVersion");
    rv = amxd_trans_set_uint32_t(&transaction, "PHYTxRate", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter PHYTxRate");
    rv = amxd_trans_set_uint32_t(&transaction, "PHYRxRate", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter PHYRxRate");
    rv = amxd_trans_set_uint32_t(&transaction, "TxPowerControlReduction", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter TxPowerControlReduction");
    rv = amxd_trans_set_uint32_t(&transaction, "RxPowerLevel", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter RxPowerLevel");
    rv = amxd_trans_set_uint32_t(&transaction, "TxBcastRate", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter TxBcastRate");
    rv = amxd_trans_set_int32_t(&transaction, "RxBcastPowerLevel", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter RxBcastPowerLevel");
    rv = amxd_trans_set_uint64_t(&transaction, "TxPackets", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter TxPackets");
    rv = amxd_trans_set_uint32_t(&transaction, "TxDrops", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter TxDrops");
    rv = amxd_trans_set_uint64_t(&transaction, "RxPackets", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter RxPackets");
    rv = amxd_trans_set_uint64_t(&transaction, "RxCorrected", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter RxCorrected");
    rv = amxd_trans_set_uint32_t(&transaction, "RxErroredAndMissedPackets", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter RxErroredAndMissedPackets");
    rv = amxd_trans_set_bool(&transaction, "BondingCapable", false);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter BondingCapable");
    rv = amxd_trans_set_bool(&transaction, "QAM256Capable", false);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter QAM256Capable");
    rv = amxd_trans_set_uint32_t(&transaction, "PacketAggregationCapability", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter PacketAggregationCapability");
    rv = amxd_trans_set_uint32_t(&transaction, "RxSNR", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter RxSNR");
    rv = amxd_trans_set_bool(&transaction, "Active", false);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter Active");
    rv = amxd_trans_set_uint32_t(&transaction, "SupportedIngressPqosFlows", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter SupportedIngressPqosFlows");
    rv = amxd_trans_set_uint32_t(&transaction, "SupportedEgressPqosFlows", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter SupportedEgressPqosFlows");
    rv = amxd_trans_set_uint32_t(&transaction, "AggregationSize", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter AggregationSize");
    rv = amxd_trans_set_uint32_t(&transaction, "AeNumber", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter AeNumber");
    rv = amxd_trans_set_cstring_t(&transaction, "PowerState", "");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter PowerState");
    rv = amxd_trans_set_cstring_t(&transaction, "PowerStateCapability", "");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter PowerStateCapability");
    rv = amxd_trans_set_bool(&transaction, "Moca25PhyCapable", false);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter Moca25PhyCapable");

    rv = amxd_trans_apply(&transaction, dm);
    when_failed_trace(rv, exit, ERROR, "failed to apply transaction");

    SAH_TRACEZ_INFO(ME, "Added node instance %d on interface %s", id, amxd_object_get_name(mocaif_obj, AMXD_OBJECT_NAMED));

exit:
    amxd_trans_clean(&transaction);
    return rv;
}

static amxd_status_t add_mesh_entry(amxd_object_t* mocaif_obj, uint32_t id) {
    amxd_status_t rv;
    amxd_dm_t* dm = amxd_object_get_dm(mocaif_obj);
    amxd_object_t* meshs_obj = amxd_object_get_child(mocaif_obj, "Mesh");
    amxd_trans_t transaction;

    rv = amxd_trans_init(&transaction);
    when_failed_trace(rv, exit, ERROR, "failed to init a transaction object");
    rv = amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    when_failed_trace(rv, exit, ERROR, "failed to set the transaction attribute");

    rv = amxd_trans_select_object(&transaction, meshs_obj);
    when_failed_trace(rv, exit, ERROR, "failed to select an object");
    rv = amxd_trans_add_inst(&transaction, id, NULL);
    when_failed_trace(rv, exit, ERROR, "failed to add instance to transaction");

    rv = amxd_trans_set_uint32_t(&transaction, "TxNodeIndex", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter TxNodeIndex");
    rv = amxd_trans_set_uint32_t(&transaction, "RxNodeIndex", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter RxNodeIndex");
    rv = amxd_trans_set_uint32_t(&transaction, "TxRate", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter TxRate");
    rv = amxd_trans_set_uint32_t(&transaction, "TxRateNper", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter TxRateNper");
    rv = amxd_trans_set_uint32_t(&transaction, "TxRateVlper", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter TxRateVlper");

    rv = amxd_trans_apply(&transaction, dm);
    when_failed_trace(rv, exit, ERROR, "failed to apply transaction");

    SAH_TRACEZ_INFO(ME, "Added mesh instance %d on interface %s", id, amxd_object_get_name(mocaif_obj, AMXD_OBJECT_NAMED));

exit:
    amxd_trans_clean(&transaction);
    return rv;
}

static amxd_status_t add_bridge_entry(amxd_object_t* mocaif_obj, uint32_t id) {
    amxd_status_t rv;
    amxd_dm_t* dm = amxd_object_get_dm(mocaif_obj);
    amxd_object_t* bridges_obj = amxd_object_get_child(mocaif_obj, "Bridge");
    amxd_trans_t transaction;

    rv = amxd_trans_init(&transaction);
    when_failed_trace(rv, exit, ERROR, "failed to init a transaction object");
    rv = amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    when_failed_trace(rv, exit, ERROR, "failed to set the transaction attribute");

    rv = amxd_trans_select_object(&transaction, bridges_obj);
    when_failed_trace(rv, exit, ERROR, "failed to select an object");
    rv = amxd_trans_add_inst(&transaction, id, NULL);
    when_failed_trace(rv, exit, ERROR, "failed to add instance to transaction");

    rv = amxd_trans_set_uint32_t(&transaction, "NodeIndex", 0);
    when_failed_trace(rv, exit, ERROR, "failed to init parameter NodeIndex");
    rv = amxd_trans_set_csv_string_t(&transaction, "MACAddresses", "");
    when_failed_trace(rv, exit, ERROR, "failed to init parameter MACAddresses");

    rv = amxd_trans_apply(&transaction, dm);
    when_failed_trace(rv, exit, ERROR, "failed to apply transaction");

    SAH_TRACEZ_INFO(ME, "Added bridge instance %d on interface %s", id, amxd_object_get_name(mocaif_obj, AMXD_OBJECT_NAMED));

exit:
    amxd_trans_clean(&transaction);
    return rv;
}

void _moca_interface_added(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    amxd_status_t rv = amxd_status_invalid_function_argument;
    amxd_dm_t* dm = moca_get_dm();
    moca_info_t* moca = NULL;
    amxd_object_t* interface_templ_obj = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* interface_obj = NULL;
    const char* intf_name = GETP_CHAR(data, "parameters.InternalName");
    const char* netdev_ifname = GETP_CHAR(data, "parameters.Name");
    const bool hwpresent = GETP_BOOL(data, "parameters.HWPresent");
    amxc_var_t if_data;
    amxc_var_t ret;

    when_null_trace(interface_templ_obj, exit, ERROR, "Could not get template object");
    interface_obj = amxd_object_get_instance(interface_templ_obj, NULL, GET_UINT32(data, "index"));
    when_null_trace(interface_obj, exit, ERROR, "Could not get the added instance");

    when_not_null(interface_obj->priv, exit);

    SAH_TRACEZ_INFO(ME, "Adding MoCA interface %s netdev: %s: hwpresent=%d", intf_name != NULL ? intf_name : "",
                    netdev_ifname != NULL ? netdev_ifname : "", hwpresent);

    rv = moca_info_new(&moca, interface_obj, intf_name, netdev_ifname, hwpresent);
    when_failed_trace(rv, exit, ERROR, "Could not create moca info");

    interface_obj->priv = moca;

    netdev_link_state_subscription_new(moca);

    SAH_TRACEZ_NOTICE(ME, "%s, with netdev %s added", intf_name != NULL ? intf_name : "",
                      netdev_ifname != NULL ? netdev_ifname : "");

    when_failed_trace(amxc_var_init(&if_data), exit, ERROR, "Failed to init if_data");
    when_failed_trace(amxc_var_init(&ret), exit, ERROR, "Failed to init ret");

    if(hwpresent) {
        bool enable = GETP_BOOL(data, "parameters.Enable");
        int rc = -1;

        amxc_var_set_type(&if_data, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &if_data, "InternalName", moca->name_intf);
        amxc_var_add_key(cstring_t, &if_data, "Name", moca->netdev_intf);

        rc = moca_interface_create(moca, &if_data, &ret);
        when_failed_trace(rc, clean, ERROR, "Failed to create moca interface %s: %d",
                          moca->name_intf, rc);

        rc = moca_interface_set_enabled(moca, enable);
        when_failed_trace(rc, clean, ERROR, "Failed to %s moca interface %s: %d",
                          enable ? "enable" : "disable", moca->name_intf, rc);
    } else {
        dm_moca_update_status(moca, "NotPresent");
    }

    for(uint32_t i = 0; i < MOCA_MAX_NODES; i++) {
        when_failed_trace(add_node_entry(interface_obj, i + 1), exit, ERROR,
                          "Could not create node instance for node_id %d on %s", i, moca->name_intf);
        for(uint32_t j = i * MOCA_MAX_NODES; j < ((i + 1) * MOCA_MAX_NODES); j++) {
            // No entries with the same tx_node_id and rx_node_id
            if((j % MOCA_MAX_NODES) == i) {
                continue;
            }
            when_failed_trace(add_mesh_entry(interface_obj, j + 1), exit, ERROR,
                              "Could not create mesh instance %d for node_id %d on %s", j, i, moca->name_intf);

        }
        when_failed_trace(add_bridge_entry(interface_obj, i + 1), exit, ERROR,
                          "Could not create bridge instance for node_id %d on %s", i, moca->name_intf);
    }

    for(uint32_t i = 0; i < MOCA_MAX_FLOWS; i++) {
        when_failed_trace(add_flow_entry(interface_obj, i + 1), exit, ERROR,
                          "Could not create flow instance for flow_id %d on %s", i, moca->name_intf);
    }


clean:
    amxc_var_clean(&if_data);
    amxc_var_clean(&ret);
exit:
    return;
}

void _moca_interface_enable_changed(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    int ret = -1;
    amxd_object_t* obj = amxd_dm_signal_get_object(moca_get_dm(), data);
    moca_info_t* moca = NULL;
    bool enable = GETP_BOOL(data, "parameters.Enable.to");

    when_null_trace(obj, exit, ERROR, "Could not get object");
    moca = (moca_info_t*) obj->priv;
    when_null_trace(moca, exit, ERROR, "Object private data is null");
    SAH_TRACEZ_INFO(ME, "MoCA interface %s changed to %s", moca->name_intf, enable ? "enabled" : "disabled");
    ret = moca_interface_set_enabled(moca, enable);
    when_failed_trace(ret, exit, ERROR, "Failed to %s moca interface %s: %d",
                      enable ? "enable" : "disable", moca->name_intf, ret);

    if(GETP_BOOL(data, "parameters.Enable.to")) {
        dm_moca_update_status(moca, "Up");
    } else {
        dm_moca_update_status(moca, "Down");
    }

exit:
    return;
}

void _moca_interface_hwpresent_changed(UNUSED const char* const sig_name,
                                       const amxc_var_t* const data,
                                       UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(moca_get_dm(), data);
    moca_info_t* moca = NULL;
    amxc_var_t if_data;
    amxc_var_t ret;
    bool present = GETP_BOOL(data, "parameters.HWPresent.to");

    when_null_trace(obj, exit, ERROR, "Could not get object");
    moca = (moca_info_t*) obj->priv;
    when_null_trace(moca, exit, ERROR, "Object private data is null");

    when_failed_trace(amxc_var_init(&if_data), exit, ERROR, "Failed to init if_data");
    when_failed_trace(amxc_var_init(&ret), exit, ERROR, "Failed to init ret");

    SAH_TRACEZ_INFO(ME, "MoCA interface %s presence changed to %s present", moca->name_intf, present ? "" : "not");
    moca->present = present;

    amxc_var_set_type(&if_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &if_data, "InternalName", moca->name_intf);
    amxc_var_add_key(cstring_t, &if_data, "Name", moca->netdev_intf);


    if(present) {
        bool enable = GET_BOOL(amxd_object_get_param_value(obj, "Enable"), 0);
        int rc = -1;

        SAH_TRACEZ_INFO(ME, "Creating interface name: %s netdev_name: %s: set to %s", moca->name_intf, moca->netdev_intf, enable ? "enabled" : "disabled");

        rc = moca_interface_create(moca, &if_data, &ret);
        when_failed_trace(rc, clean, ERROR, "Failed to create moca interface %s: %d",
                          moca->name_intf, rc);

        rc = moca_interface_set_enabled(moca, enable);
        when_failed_trace(rc, clean, ERROR, "Failed to %s moca interface %s: %d",
                          enable ? "enable" : "disable", moca->name_intf, rc);
    } else {
        when_failed_trace(mod_moca_execute_function(moca->object, "destroy-moca-iface", &if_data, &ret),
                          clean, ERROR, "destroy-moca-iface failed");
        dm_moca_update_status(moca, "NotPresent");
    }

clean:
    amxc_var_clean(&if_data);
    amxc_var_clean(&ret);
exit:
    return;
}

void _print_event(UNUSED const char* const event_name,
                  const amxc_var_t* const event_data,
                  UNUSED void* const priv) {
    if(sahTraceZoneLevel(sahTraceGetZone(ME)) >= TRACE_LEVEL_INFO) {
        SAH_TRACEZ_INFO(ME, "received event: %s", event_name);
        amxc_var_log(event_data);
    }
}
