/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOCA_ACTIONS_H__)
#define __MOCA_ACTIONS_H__

#ifdef __cplusplus
extern "C"
{
#endif

amxd_status_t _check_is_empty_or_in(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv);

amxd_status_t _lastchange_on_read(amxd_object_t* const object,
                                  amxd_param_t* const param,
                                  amxd_action_t reason,
                                  const amxc_var_t* const args,
                                  amxc_var_t* const retval,
                                  void* priv);

amxd_status_t _cfg_info_read(amxd_object_t* const object,
                             amxd_param_t* const param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv);

amxd_status_t _cfg_info_write(amxd_object_t* const object,
                              amxd_param_t* const param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv);

void moca_cfg_info_fill(amxd_object_t* mocaif_obj, amxc_var_t* cfg_info);
amxd_object_t* moca_cfg_info_fetch(amxd_object_t* object);
amxd_object_t* moca_cfg_info_push(amxd_object_t* object, amxd_param_t* const param, const amxc_var_t* const new_value);

amxd_status_t _static_info_read(amxd_object_t* const object,
                                amxd_param_t* const param,
                                amxd_action_t reason,
                                const amxc_var_t* const args,
                                amxc_var_t* const retval,
                                void* priv);

void moca_static_info_fill(amxd_object_t* mocaif_obj, amxc_var_t* static_info);
amxd_object_t* moca_static_info_fetch(amxd_object_t* object);

amxd_status_t _dyn_info_read(amxd_object_t* const object,
                             amxd_param_t* const param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv);

void moca_dyn_info_fill(amxd_object_t* mocaif_obj, amxc_var_t* dyn_info);
amxd_object_t* moca_dyn_info_fetch(amxd_object_t* object);

amxd_status_t _qos_info_read(amxd_object_t* const object,
                             amxd_param_t* const param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv);

void moca_qos_info_fill(amxd_object_t* mocaif_obj, amxc_var_t* qos_info);
amxd_object_t* moca_qos_info_fetch(amxd_object_t* object);

amxd_status_t _rlapm_info_read(amxd_object_t* const object,
                               amxd_param_t* const param,
                               amxd_action_t reason,
                               const amxc_var_t* const args,
                               amxc_var_t* const retval,
                               void* priv);

void moca_rlapm_info_fill(amxd_object_t* mocaif_obj, amxc_var_t* rlapm_info);
amxd_object_t* moca_rlapm_info_fetch(amxd_object_t* object);

amxd_status_t _sapm_info_read(amxd_object_t* const object,
                              amxd_param_t* const param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv);

void moca_sapm_info_fill(amxd_object_t* mocaif_obj, amxc_var_t* sapm_info);
amxd_object_t* moca_sapm_info_fetch(amxd_object_t* object);

amxd_status_t _flow_info_read(amxd_object_t* const object,
                              amxd_param_t* const param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv);

void moca_flow_info_fill(amxd_object_t* mocaif_obj, amxc_var_t* flow_info);
amxd_object_t* moca_flow_info_fetch(amxd_object_t* object);

amxd_status_t _node_info_read(amxd_object_t* const object,
                              amxd_param_t* const param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv);

void moca_node_info_fill(amxd_object_t* mocaif_obj, amxc_var_t* node_info);
amxd_object_t* moca_node_info_fetch(amxd_object_t* object);

amxd_status_t _mesh_info_read(amxd_object_t* const object,
                              amxd_param_t* const param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv);

void moca_mesh_info_fill(amxd_object_t* mocaif_obj, amxc_var_t* mesh_info);
amxd_object_t* moca_mesh_info_fetch(amxd_object_t* object);

amxd_status_t _bridge_info_read(amxd_object_t* const object,
                                amxd_param_t* const param,
                                amxd_action_t reason,
                                const amxc_var_t* const args,
                                amxc_var_t* const retval,
                                void* priv);

void moca_bridge_info_fill(amxd_object_t* mocaif_obj, amxc_var_t* bridge_info);
amxd_object_t* moca_bridge_info_fetch(amxd_object_t* object);


amxd_status_t _moca_instance_removed(amxd_object_t* object,
                                     amxd_param_t* param,
                                     amxd_action_t reason,
                                     const amxc_var_t* const args,
                                     amxc_var_t* const retval,
                                     void* priv);


#ifdef __cplusplus
}
#endif

#endif // __MOCA_ACTIONS_H__
