%populate {
    object MoCA.Interface {
{% for (let Interface in BD.Interfaces ):  if ( Interface.Type == "moca" ):  %}
        instance add (Alias = "{{Interface.Alias}}", Name = "{{Interface.Name}}") {
            parameter InternalName = "{{Interface.InternalName}}";
            parameter Enable = "true";
            parameter Band = "bandExD";
            parameter MaxNodes = "true";
        }
{% endif; endfor; %}
    }
}
