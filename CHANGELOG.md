# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.6.1 - 2024-12-06(13:38:34 +0000)

### Fixes

- - Support dynamic adding/removal of interfaces

## Release v0.6.0 - 2024-11-22(09:57:46 +0000)

### New

- - Support dynamic adding/removal of interfaces

## Release v0.5.17 - 2024-11-21(12:38:40 +0000)

### Other

- The netmodel-moca client fails to start synchronizing at

## Release v0.5.16 - 2024-10-23(12:05:31 +0000)

### Fixes

- - netdevice displays incorrect data when COAX cable is disconnected

## Release v0.5.15 - 2024-10-22(06:40:36 +0000)

### Fixes

- - MAC address discrepancy for Coax network

## Release v0.5.14 - 2024-10-01(11:16:44 +0000)

### Other

- - Add description parameter for MoCA Interface

## Release v0.5.13 - 2024-09-10(07:17:15 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.5.12 - 2024-07-28(06:30:04 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v0.5.11 - 2024-07-16(06:27:53 +0000)

### Other

- moca-manager must be registered with PCM

## Release v0.5.10 - 2024-05-29(07:14:59 +0000)

### Fixes

- - Definition of MoCA's InterfaceName to 'ethx' leads to miconfiguration of the bridge

## Release v0.5.9 - 2024-05-15(09:25:10 +0000)

### Other

- - [MoCA][DM] MoCA status is disabled under DM while devices can...

## Release v0.5.8 - 2024-04-10(09:59:45 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.5.7 - 2024-03-28(12:37:38 +0000)

### Fixes

- - [tr181-Moca] Moca plugin is not started after normal boot

## Release v0.5.6 - 2024-03-26(12:41:47 +0000)

### Other

- - TR-181: MoCA Data model issues

## Release v0.5.5 - 2024-03-25(08:55:13 +0000)

### Other

- [MOCA] Remove space in Moca odl file

## Release v0.5.4 - 2024-03-22(16:13:57 +0000)

### Fixes

- Interface.Name is an unique key

## Release v0.5.3 - 2024-03-22(11:20:14 +0000)

### Fixes

- install ucode defaults template

## Release v0.5.2 - 2024-03-21(16:36:28 +0000)

### Changes

- change default boot order to 77

## Release v0.5.1 - 2024-03-18(14:58:26 +0000)

### Fixes

- - Rename the mod_moca-mxl to mod-moca-mxl for consistency with other modules

## Release v0.5.0 - 2024-02-05(16:21:02 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.4.0 - 2024-01-17(09:32:29 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.3.5 - 2023-12-05(12:12:04 +0000)

### Other

- - Add support of the next version of BBF MoCA DataModel

## Release v0.3.4 - 2023-11-20(20:11:13 +0000)

### Other

- [odl-generator] Add support for Moca interfaces

## Release v0.3.3 - 2023-04-05(16:40:38 +0000)

### Fixes

- moca-manager *.LastChanges are wrong in datamodel

## Release v0.3.2 - 2023-03-15(10:57:05 +0000)

### Fixes

- Moca NetModel IPv4 and IPv6 flag set on wrong interface

## Release v0.3.1 - 2023-03-09(09:19:34 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v0.3.0 - 2023-02-16(10:32:49 +0000)

### New

- Add AssociatedDevice Table support

## Release v0.2.5 - 2023-01-17(08:26:03 +0000)

### Other

- MoCA interfaces must be synchronized with Netmodel

## Release v0.2.4 - 2023-01-14(08:35:34 +0000)

### Other

- - MoCA interfaces must be synchronized with Netmodel

## Release v0.2.3 - 2022-12-19(16:09:24 +0000)

### Other

- - Standardization of MoCa Low-Level API for prpl

## Release v0.2.2 - 2022-12-09(09:29:36 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.2.1 - 2022-06-10(14:35:51 +0000)

### Other

- [amxrt] All amx plugins should start with the -D option

## Release v0.2.0 - 2022-03-21(10:53:46 +0000)

### New

- [MoCa] [debug] It must be possible to gather specific MoCa debug information.

## Release v0.1.3 - 2022-02-25(11:45:13 +0000)

### Other

- Enable core dumps by default

## Release v0.1.2 - 2021-12-21(11:46:23 +0000)

### Fixes

- Stats must not be persistent

## Release v0.1.1 - 2021-11-29(07:51:24 +0000)

### Fixes

- Fix loading of datamodel

## Release v0.1.0 - 2021-11-24(09:29:28 +0000)

### New

- First delivery with initial datamodel

