/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_moca-dummy.h"

#define ME "dummy"

static amxc_llist_t* dummy_moca_ifaces = NULL;

static int init_dummy_moca_iface(dummy_moca_iface_t* moca_if, const char* name_intf, const char* netdev_name) {
    int rv = -1;
    int i, j = 0;

    SAH_TRACEZ_INFO(ME, "initializing dummy moca info for name: %s", name_intf);
    when_null_trace(moca_if, exit, ERROR, "interface can not be null, invalid argument");
    when_str_empty(name_intf, exit);

    if(!strcmp("moca0", name_intf)) {
        strcpy(moca_if->name, name_intf);
        strcpy(moca_if->netdev_name, netdev_name);
        strcpy(moca_if->mac_addr, "00:11:22:33:44:55");
        strcpy(moca_if->firmware_version, "Firmware Version 2.5");
        strcpy(moca_if->supported_bands, "bandFCBL,bandFSAT,bandE,bandExD");
        moca_if->maxbitrate = 2500000;
        moca_if->aggr_size = 80;
        moca_if->max_ae_num = 10;
        moca_if->max_ingress_pqos_flows = MOCA_MAX_FLOWS;
        moca_if->max_egress_pqos_flows = MOCA_MAX_FLOWS;
        strcpy(moca_if->highest_version, "2.5");
        strcpy(moca_if->freq_cap_mask, "000000FF1FFFC0FF");
        strcpy(moca_if->net_taboo_mask, "DEADBEEFdeadbeef");
        moca_if->tx_bcast_power_reduction = 50;
        moca_if->qam256_capable = true;
        moca_if->avb_capable = false;
        moca_if->packet_aggr_capability = 80;
        moca_if->enable = true;
        moca_if->preferred_nc = true;
        moca_if->privacy_enabled = false;
        moca_if->tpc_phy_rate_nper = 680;
        strcpy(moca_if->freq_cur_mask, "000000001FFFC000");
        strcpy(moca_if->keyphrase, "123456789012");
        moca_if->tx_power_limit = 10;
        moca_if->power_cntl_phy_target = 20;
        moca_if->beacon_power_limit = 30;
        strcpy(moca_if->node_taboo_mask, "deadbeefDEADBEEF");
        moca_if->rlapm_num = MOCA_MAX_RLAPM_ENTRIES;
        for(i = 0; i < MOCA_MAX_RLAPM_ENTRIES; i++) {
            moca_if->rlapm[i].enable = 1;
            moca_if->rlapm[i].profile = 0;
            moca_if->rlapm[i].freq = 850;
            moca_if->rlapm[i].garpl = i;
            strcpy(moca_if->rlapm[i].status, "active");
        }
        moca_if->sapm_num = MOCA_MAX_SAPM_ENTRIES;
        for(i = 0; i < MOCA_MAX_SAPM_ENTRIES; i++) {
            char phy_margin[8];
            int len;

            moca_if->sapm[i].enable = 1;
            moca_if->sapm[i].profile = 0;
            moca_if->sapm[i].freq = 700;
            strcpy(moca_if->sapm[i].phy_margin, "");
            for(j = 0; j < MOCA_MAX_SAPM_SUBCARRIER_ENTRIES; j++) {
                sprintf(phy_margin, "%d,", j);
                strcat(moca_if->sapm[i].phy_margin, phy_margin);
            }
            len = strlen(moca_if->sapm[i].phy_margin);
            if(len > 0) {
                // Trim trailing comma
                moca_if->sapm[i].phy_margin[len - 1] = '\0';
            }
            strcpy(moca_if->sapm[i].status, "active");
        }
        strcpy(moca_if->status, "Up");
        strcpy(moca_if->pass_hash, "ABCD");
        strcpy(moca_if->power_state_capability, "m0Active");
        moca_if->linkup_time = 3600;
        moca_if->max_ingress_bw = 2500000;
        moca_if->max_egress_bw = 2500000;
        strcpy(moca_if->current_version, "2.5");
        moca_if->network_coord = 0;
        moca_if->node_id = 0;
        moca_if->backup_nc = 1,
        moca_if->num_nodes = 3,
        moca_if->cur_oper_freq = 1150;
        moca_if->last_oper_freq = 1400;
        moca_if->tx_bcast_rate = 2500000;
        moca_if->reset_count = 2;
        moca_if->link_down_count = 5;
        moca_if->lmo_node_id = 1;
        strcpy(moca_if->network_state, "steadyState");
        moca_if->primary_chan_offset = 25;
        moca_if->secondary_chan_offset = -125;
        strcpy(moca_if->reset_reason, "MoCA Reset reason");
        strcpy(moca_if->nc_version, "2.5");
        strcpy(moca_if->link_state, "ABCDEF");
        moca_if->ingress_num_flows = 2;
        moca_if->egress_num_flows = 2;
        strcpy(moca_if->flows[0].flow_id, "01:00:5e:00:01:06");
        strcpy(moca_if->flows[0].packet_da, "01:00:5e:00:01:06");
        strcpy(moca_if->flows[0].egress_guid, "ac:91:9b:3a:5f:4a");
        strcpy(moca_if->flows[0].ingress_guid, "94:cc:04:11:32:b4");
        moca_if->flows[0].max_rate = 1000;
        moca_if->flows[0].max_burst_size = 2;
        moca_if->flows[0].dfid = 128;
        moca_if->flows[0].lease_time_left = 4294967295;
        strcpy(moca_if->flows[1].flow_id, "01:00:5e:00:01:00");
        strcpy(moca_if->flows[1].packet_da, "01:00:5e:00:01:00");
        strcpy(moca_if->flows[1].egress_guid, "ac:91:9b:0d:66:df");
        strcpy(moca_if->flows[1].ingress_guid, "ac:91:9b:3a:5f:4a");
        moca_if->flows[1].max_rate = 1000;
        moca_if->flows[1].max_burst_size = 2;
        moca_if->flows[1].dfid = 128;
        moca_if->flows[1].lease_time_left = 4294967295;
        strcpy(moca_if->nodes[0].mac_addr, "00:11:22:33:44:55");
        moca_if->nodes[0].node_id = 0;
        moca_if->nodes[0].preferred_nc = true;
        strcpy(moca_if->nodes[0].highest_version, "2.5");
        moca_if->nodes[0].phy_tx_rate = 0;
        moca_if->nodes[0].phy_rx_rate = 0;
        moca_if->nodes[0].tx_power_control_reduction = 0;
        moca_if->nodes[0].rx_power_level = 0;
        moca_if->nodes[0].tx_bcast_rate = 2500000;
        moca_if->nodes[0].rx_bcast_power_level = 0;
        moca_if->nodes[0].tx_packets = 9999;
        moca_if->nodes[0].tx_drops = 11;
        moca_if->nodes[0].rx_packets = 8888;
        moca_if->nodes[0].rx_corrected = 1111;
        moca_if->nodes[0].rx_errored_and_missed_packets = 1111;
        moca_if->nodes[0].bonding_capable = true;
        moca_if->nodes[0].qam256_capable = true;
        moca_if->nodes[0].packet_aggr_capability = 80;
        moca_if->nodes[0].rx_snr = 0;
        moca_if->nodes[0].max_ingress_pqos_flows = 5;
        moca_if->nodes[0].max_egress_pqos_flows = 7;
        moca_if->nodes[0].aggr_size = 80;
        moca_if->nodes[0].max_ae_num = 10;
        strcpy(moca_if->nodes[0].power_state, "m0Active");
        strcpy(moca_if->nodes[0].power_state_capability, "m0Active");
        moca_if->nodes[0].active = true;
        moca_if->nodes[0].moca25_phy_capable = true;
        strcpy(moca_if->nodes[1].mac_addr, "66:77:88:99:aa:bb");
        moca_if->nodes[1].node_id = 1;
        moca_if->nodes[1].preferred_nc = false;
        strcpy(moca_if->nodes[1].highest_version, "2.0");
        moca_if->nodes[1].phy_tx_rate = 0;
        moca_if->nodes[1].phy_rx_rate = 0;
        moca_if->nodes[1].tx_power_control_reduction = 0;
        moca_if->nodes[1].rx_power_level = 0;
        moca_if->nodes[1].tx_bcast_rate = 2000000;
        moca_if->nodes[1].rx_bcast_power_level = 0;
        moca_if->nodes[1].tx_packets = 7777;
        moca_if->nodes[1].tx_drops = 222;
        moca_if->nodes[1].rx_packets = 6666;
        moca_if->nodes[1].rx_corrected = 2222;
        moca_if->nodes[1].rx_errored_and_missed_packets = 1111;
        moca_if->nodes[1].bonding_capable = false;
        moca_if->nodes[1].qam256_capable = false;
        moca_if->nodes[1].packet_aggr_capability = 20;
        moca_if->nodes[1].rx_snr = 0;
        moca_if->nodes[1].max_ingress_pqos_flows = 3;
        moca_if->nodes[1].max_egress_pqos_flows = 5;
        moca_if->nodes[1].aggr_size = 20;
        moca_if->nodes[1].max_ae_num = 5;
        strcpy(moca_if->nodes[1].power_state, "m1LowPowerIdle");
        strcpy(moca_if->nodes[1].power_state_capability, "m0Active");
        moca_if->nodes[1].active = true;
        moca_if->nodes[1].moca25_phy_capable = false;
        strcpy(moca_if->nodes[2].mac_addr, "cc:dd:ee:ff:00:11");
        moca_if->nodes[2].node_id = 2;
        moca_if->nodes[2].preferred_nc = false;
        strcpy(moca_if->nodes[2].highest_version, "1.0");
        moca_if->nodes[2].phy_tx_rate = 100000;
        moca_if->nodes[2].phy_rx_rate = 100000;
        moca_if->nodes[2].tx_power_control_reduction = 0;
        moca_if->nodes[2].rx_power_level = 0;
        moca_if->nodes[2].tx_bcast_rate = 10000;
        moca_if->nodes[2].rx_bcast_power_level = 0;
        moca_if->nodes[2].tx_packets = 5555;
        moca_if->nodes[2].tx_drops = 333;
        moca_if->nodes[2].rx_packets = 4444;
        moca_if->nodes[2].rx_corrected = 3333;
        moca_if->nodes[2].rx_errored_and_missed_packets = 1111;
        moca_if->nodes[2].bonding_capable = false;
        moca_if->nodes[2].qam256_capable = false;
        moca_if->nodes[2].packet_aggr_capability = 6;
        moca_if->nodes[2].rx_snr = 0;
        moca_if->nodes[2].max_ingress_pqos_flows = 2;
        moca_if->nodes[2].max_egress_pqos_flows = 3;
        moca_if->nodes[2].aggr_size = 6;
        moca_if->nodes[2].max_ae_num = 3;
        strcpy(moca_if->nodes[2].power_state, "m2Standby");
        strcpy(moca_if->nodes[2].power_state_capability, "m0Active");
        moca_if->nodes[2].active = true;
        moca_if->nodes[2].moca25_phy_capable = false;
        for(i = 0; i < MOCA_MAX_NODES; i++) {
            for(j = 0; j < MOCA_MAX_NODES; j++) {
                moca_if->meshes[i].entry[j].tx_rate = 2000000;
                moca_if->meshes[i].entry[j].tx_rate_nper = 2000000;
                moca_if->meshes[i].entry[j].tx_rate_vlper = 2000000;
            }
        }
        moca_if->bridges[0].node_id = 0;
        sprintf(moca_if->bridges[0].mac_addresses, "00:11:22:33:44:51,00:11:22:33:44:52,00:11:22:33:44:52");
        moca_if->bridges[1].node_id = 1;
        sprintf(moca_if->bridges[1].mac_addresses, "66:77:88:99:aa:bd,66:77:88:99:aa:be,66:77:88:99:aa:bf");
        moca_if->bridges[2].node_id = 2;
        sprintf(moca_if->bridges[2].mac_addresses, "cc:dd:ee:ff:00:11,cc:dd:ee:ff:00:12,cc:dd:ee:ff:00:13");
    } else if(!strcmp("moca1", name_intf)) {
        strcpy(moca_if->name, name_intf);
        strcpy(moca_if->netdev_name, netdev_name);
        strcpy(moca_if->mac_addr, "66:77:88:99:aa:bb");
        strcpy(moca_if->firmware_version, "Firmware Version 2.0");
        strcpy(moca_if->supported_bands, "bandDH,bandDL");
        moca_if->maxbitrate = 2000000;
        moca_if->aggr_size = 20;
        moca_if->max_ae_num = 7;
        moca_if->max_ingress_pqos_flows = MOCA_MAX_FLOWS;
        moca_if->max_egress_pqos_flows = MOCA_MAX_FLOWS;
        strcpy(moca_if->highest_version, "2.0");
        strcpy(moca_if->freq_cap_mask, "000000FF1FFFC0FF");
        strcpy(moca_if->net_taboo_mask, "DEADBEEFdeadbeef");
        moca_if->tx_bcast_power_reduction = 40;
        moca_if->qam256_capable = false;
        moca_if->avb_capable = true;
        moca_if->packet_aggr_capability = 20;
        moca_if->enable = true;
        moca_if->preferred_nc = false;
        moca_if->privacy_enabled = true;
        moca_if->tpc_phy_rate_nper = 690;
        strcpy(moca_if->freq_cur_mask, "000000001FFFC000");
        strcpy(moca_if->keyphrase, "123456789012");
        moca_if->tx_power_limit = 11;
        moca_if->power_cntl_phy_target = 21;
        moca_if->beacon_power_limit = 31;
        strcpy(moca_if->node_taboo_mask, "deadbeefDEADBEEF");
        moca_if->rlapm_num = MOCA_MAX_RLAPM_ENTRIES;
        for(i = 0; i < MOCA_MAX_RLAPM_ENTRIES; i++) {
            moca_if->rlapm[i].enable = 1;
            moca_if->rlapm[i].profile = 0;
            moca_if->rlapm[i].freq = 850;
            moca_if->rlapm[i].garpl = i;
            strcpy(moca_if->rlapm[i].status, "active");
        }
        moca_if->sapm_num = MOCA_MAX_SAPM_ENTRIES;
        for(i = 0; i < MOCA_MAX_SAPM_ENTRIES; i++) {
            moca_if->sapm[i].enable = 1;
            moca_if->sapm[i].profile = 0;
            moca_if->sapm[i].freq = 700;
            char phy_margin[8];
            int len;
            for(j = 0; j < MOCA_MAX_SAPM_SUBCARRIER_ENTRIES; j++) {
                sprintf(phy_margin, "%d,", j);
                strcat(moca_if->sapm[i].phy_margin, phy_margin);
            }
            len = strlen(moca_if->sapm[i].phy_margin);
            if(len > 0) {
                // Trim trailing comma
                moca_if->sapm[i].phy_margin[len - 1] = '\0';
            }
            strcpy(moca_if->sapm[i].status, "active");
        }
        strcpy(moca_if->status, "Up");
        strcpy(moca_if->pass_hash, "FEDC");
        strcpy(moca_if->power_state_capability, "m0Active");
        moca_if->linkup_time = 1;
        moca_if->max_ingress_bw = 2000000;
        moca_if->max_egress_bw = 2000000;
        strcpy(moca_if->current_version, "2.0");
        moca_if->network_coord = 0;
        moca_if->node_id = 2;
        moca_if->backup_nc = 1,
        moca_if->num_nodes = 2,
        moca_if->cur_oper_freq = 1150;
        moca_if->last_oper_freq = 1125;
        moca_if->tx_bcast_rate = 2000000;
        moca_if->reset_count = 20;
        moca_if->link_down_count = 50;
        moca_if->lmo_node_id = 2;
        strcpy(moca_if->network_state, "beginLmoPhyProfileState");
        moca_if->primary_chan_offset = -25;
        moca_if->secondary_chan_offset = 125;
        strcpy(moca_if->reset_reason, "Unknown MoCA Reset reason");
        strcpy(moca_if->nc_version, "2.0");
        strcpy(moca_if->link_state, "FEDCBA");
        moca_if->ingress_num_flows = 2;
        moca_if->egress_num_flows = 2;
        strcpy(moca_if->flows[0].flow_id, "01:00:5e:00:01:06");
        strcpy(moca_if->flows[0].packet_da, "01:00:5e:00:01:06");
        strcpy(moca_if->flows[0].egress_guid, "ac:91:9b:3a:5f:4a");
        strcpy(moca_if->flows[0].ingress_guid, "94:cc:04:11:32:b4");
        moca_if->flows[0].max_rate = 1000;
        moca_if->flows[0].max_burst_size = 2;
        moca_if->flows[0].dfid = 128;
        moca_if->flows[0].lease_time_left = 4294967295;
        strcpy(moca_if->flows[1].flow_id, "01:00:5e:00:01:00");
        strcpy(moca_if->flows[1].packet_da, "01:00:5e:00:01:00");
        strcpy(moca_if->flows[1].egress_guid, "ac:91:9b:0d:66:df");
        strcpy(moca_if->flows[1].ingress_guid, "ac:91:9b:3a:5f:4a");
        moca_if->flows[1].max_rate = 1000;
        moca_if->flows[1].max_burst_size = 2;
        moca_if->flows[1].dfid = 128;
        moca_if->flows[1].lease_time_left = 4294967295;
        strcpy(moca_if->nodes[0].mac_addr, "22:33:44:55:66:77");
        moca_if->nodes[0].node_id = 0;
        moca_if->nodes[0].preferred_nc = true;
        strcpy(moca_if->nodes[0].highest_version, "2.0");
        moca_if->nodes[0].phy_tx_rate = 0;
        moca_if->nodes[0].phy_rx_rate = 0;
        moca_if->nodes[0].tx_power_control_reduction = 0;
        moca_if->nodes[0].rx_power_level = 0;
        moca_if->nodes[0].tx_bcast_rate = 2000000;
        moca_if->nodes[0].rx_bcast_power_level = 0;
        moca_if->nodes[0].tx_packets = 9999;
        moca_if->nodes[0].tx_drops = 111;
        moca_if->nodes[0].rx_packets = 8888;
        moca_if->nodes[0].rx_corrected = 1111;
        moca_if->nodes[0].rx_errored_and_missed_packets = 1111;
        moca_if->nodes[0].bonding_capable = true;
        moca_if->nodes[0].qam256_capable = false;
        moca_if->nodes[0].packet_aggr_capability = 20;
        moca_if->nodes[0].rx_snr = 0;
        moca_if->nodes[0].max_ingress_pqos_flows = 3;
        moca_if->nodes[0].max_egress_pqos_flows = 5;
        moca_if->nodes[0].aggr_size = 20;
        moca_if->nodes[0].max_ae_num = 5;
        strcpy(moca_if->nodes[0].power_state, "m3Sleep");
        strcpy(moca_if->nodes[0].power_state_capability, "m0Active");
        moca_if->nodes[0].active = true;
        moca_if->nodes[0].moca25_phy_capable = false;
        strcpy(moca_if->nodes[1].mac_addr, "cc:dd:ee:ff:00:11");
        moca_if->nodes[1].node_id = 1;
        moca_if->nodes[1].preferred_nc = false;
        strcpy(moca_if->nodes[1].highest_version, "2.0");
        moca_if->nodes[1].phy_tx_rate = 0;
        moca_if->nodes[1].phy_rx_rate = 0;
        moca_if->nodes[1].tx_power_control_reduction = 0;
        moca_if->nodes[1].rx_power_level = 0;
        moca_if->nodes[1].tx_bcast_rate = 2000000;
        moca_if->nodes[1].rx_bcast_power_level = 0;
        moca_if->nodes[1].tx_packets = 6666;
        moca_if->nodes[1].tx_drops = 222;
        moca_if->nodes[1].rx_packets = 5555;
        moca_if->nodes[1].rx_corrected = 2222;
        moca_if->nodes[1].rx_errored_and_missed_packets = 1111;
        moca_if->nodes[1].bonding_capable = false;
        moca_if->nodes[1].qam256_capable = false;
        moca_if->nodes[1].packet_aggr_capability = 0;
        moca_if->nodes[1].rx_snr = 0;
        moca_if->nodes[1].max_ingress_pqos_flows = 3;
        moca_if->nodes[1].max_egress_pqos_flows = 5;
        moca_if->nodes[1].aggr_size = 20;
        moca_if->nodes[1].max_ae_num = 5;
        strcpy(moca_if->nodes[1].power_state, "m1LowPowerIdle");
        strcpy(moca_if->nodes[1].power_state_capability, "m0Active");
        moca_if->nodes[1].active = true;
        moca_if->nodes[1].moca25_phy_capable = false;
        strcpy(moca_if->nodes[2].mac_addr, "66:77:88:99:aa:bb");
        moca_if->nodes[2].node_id = 2;
        moca_if->nodes[2].preferred_nc = false;
        strcpy(moca_if->nodes[2].highest_version, "2.0");
        moca_if->nodes[2].phy_tx_rate = 0;
        moca_if->nodes[2].phy_rx_rate = 0;
        moca_if->nodes[2].tx_power_control_reduction = 0;
        moca_if->nodes[2].rx_power_level = 0;
        moca_if->nodes[2].tx_bcast_rate = 200000;
        moca_if->nodes[2].rx_bcast_power_level = 0;
        moca_if->nodes[2].tx_packets = 5555;
        moca_if->nodes[2].tx_drops = 333;
        moca_if->nodes[2].rx_packets = 4444;
        moca_if->nodes[2].rx_corrected = 3333;
        moca_if->nodes[2].rx_errored_and_missed_packets = 1111;
        moca_if->nodes[2].bonding_capable = true;
        moca_if->nodes[2].qam256_capable = false;
        moca_if->nodes[2].packet_aggr_capability = 3;
        moca_if->nodes[2].rx_snr = 0;
        moca_if->nodes[2].max_ingress_pqos_flows = 3;
        moca_if->nodes[2].max_egress_pqos_flows = 5;
        moca_if->nodes[2].aggr_size = 20;
        moca_if->nodes[2].max_ae_num = 5;
        strcpy(moca_if->nodes[2].power_state, "m1LowPowerIdle");
        strcpy(moca_if->nodes[2].power_state_capability, "m0Active");
        moca_if->nodes[2].active = true;
        moca_if->nodes[2].moca25_phy_capable = false;
        for(i = 0; i < MOCA_MAX_NODES; i++) {
            for(j = 0; j < MOCA_MAX_NODES; j++) {
                moca_if->meshes[i].entry[j].tx_rate = 2000000;
                moca_if->meshes[i].entry[j].tx_rate_nper = 2000000;
                moca_if->meshes[i].entry[j].tx_rate_vlper = 2000000;
            }
        }
        moca_if->bridges[0].node_id = 0;
        sprintf(moca_if->bridges[0].mac_addresses, "22:33:44:55:66:78,22:33:44:55:66:79");
        moca_if->bridges[1].node_id = 1;
        sprintf(moca_if->bridges[1].mac_addresses, "cc:dd:ee:ff:00:12,cc:dd:ee:ff:00:13");
        moca_if->bridges[2].node_id = 2;
        sprintf(moca_if->bridges[2].mac_addresses, "66:77:88:99:aa:ff");
    } else {
        SAH_TRACEZ_ERROR(ME, "Unable to initialize moca interface with name: %s netdev: %s", name_intf, netdev_name);
        goto exit;
    }

    rv = 0;

exit:
    return rv;
}

int dummy_iface_create(UNUSED const char* function_name,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    int retval = -1;
    const char* if_name = NULL;
    const char* netdev_name = NULL;

    when_null_trace(args, exit, ERROR, "Passed args is NULL");

    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");
    if_name = GET_CHAR(args, "InternalName");
    when_null_trace(if_name, exit, ERROR, "Passed interface name is NULL");

    netdev_name = GET_CHAR(args, "Name");
    when_null_trace(netdev_name, exit, ERROR, "Passed netdev interface name is NULL");

    SAH_TRACEZ_INFO(ME, "creating dummy moca info for name: %s netdev:%s", if_name, netdev_name);

    dummy_moca_iface_t* moca_if = calloc(1, sizeof(dummy_moca_iface_t));
    when_null_trace(moca_if, exit, ERROR, "Unable to allocate memory");

    retval = init_dummy_moca_iface(moca_if, if_name, netdev_name);
    when_failed_trace(retval, clean, ERROR, "Failed to initialize dummy moca interface %s: netdev:%s", if_name, netdev_name);

    amxc_llist_append(dummy_moca_ifaces, &moca_if->it);

    SAH_TRACEZ_NOTICE(ME, "allocated dummy moca info for intreface %s netdev %s", if_name, netdev_name);

    retval = 0;

clean:
    if(retval != 0) {
        free(moca_if);
    }
exit:
    return retval;
}


static void dummy_iface_delete(amxc_llist_it_t* it) {
    dummy_moca_iface_t* moca_if = amxc_container_of(it, dummy_moca_iface_t, it);
    SAH_TRACEZ_NOTICE(ME, "deallocating dummy moca info for %s", moca_if->name);
    free(moca_if);
}


static dummy_moca_iface_t* find_dummy_moca_iface_by_name(const char* ifname) {
    dummy_moca_iface_t* moca_if;

    amxc_llist_for_each(it, dummy_moca_ifaces) {
        moca_if = amxc_container_of(it, dummy_moca_iface_t, it);
        if(!strcmp(moca_if->name, ifname)) {
            return moca_if;
        }
    }
    return NULL;
}

int dummy_iface_destroy(UNUSED const char* function_name,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    int retval = -1;
    const char* if_name = NULL;

    when_null_trace(args, exit, ERROR, "Passed args is NULL");

    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");
    if_name = GET_CHAR(args, "InternalName");
    when_null_trace(if_name, exit, ERROR, "Passed interface name is NULL");

    SAH_TRACEZ_INFO(ME, "destroy dummy moca info for name: %s", if_name);

    dummy_moca_iface_t* moca_if = find_dummy_moca_iface_by_name(if_name);
    when_null_trace(moca_if, exit, ERROR, "Dummy interface is not found");

    SAH_TRACEZ_NOTICE(ME, "%s:Remove from the list and free memory", if_name);
    amxc_llist_it_clean(&moca_if->it, dummy_iface_delete);

    retval = 0;

exit:
    return retval;
}

int dummy_cfg_info_set(UNUSED const char* function_name,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    int retval = -1;
    const char* if_name = NULL;
    dummy_moca_iface_t* moca_if = NULL;

    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");
    if_name = GET_CHAR(args, "InternalName");
    when_null_trace(if_name, exit, ERROR, "Passed interface name is NULL");

    moca_if = find_dummy_moca_iface_by_name(if_name);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    SAH_TRACEZ_INFO(ME, "Setting for interface: %s", moca_if->name);

    if(GET_ARG(args, "Enable")) {
        moca_if->enable = GET_BOOL(args, "Enable");
        if(moca_if->enable) {
            strcpy(moca_if->status, "Up");
        } else {
            strcpy(moca_if->status, "Down");
        }
        SAH_TRACEZ_INFO(ME, "Set enable=%d", moca_if->enable);
    } else if(GET_ARG(args, "PreferredNC")) {
        moca_if->preferred_nc = GET_BOOL(args, "PreferredNC");
        SAH_TRACEZ_INFO(ME, "Set preferred_nc=%d", moca_if->preferred_nc);
    } else if(GET_ARG(args, "PrivacyEnabledSetting")) {
        moca_if->privacy_enabled = GET_BOOL(args, "PrivacyEnabledSetting");
        SAH_TRACEZ_INFO(ME, "Set privacy_enabled=%d", moca_if->privacy_enabled);
    } else if(GET_ARG(args, "TpcTargetRateNper")) {
        moca_if->tpc_phy_rate_nper = GET_UINT32(args, "TpcTargetRateNper");
        SAH_TRACEZ_INFO(ME, "Set tpc_phy_rate_nper=%d", moca_if->tpc_phy_rate_nper);
    } else if(GET_ARG(args, "FreqCurrentMaskSetting")) {
        strncpy(moca_if->freq_cur_mask, GET_CHAR(args, "FreqCurrentMaskSetting"), 16);
        SAH_TRACEZ_INFO(ME, "Set freq_cur_mask=%s", moca_if->freq_cur_mask);
    } else if(GET_ARG(args, "KeyPassphrase")) {
        strncpy(moca_if->keyphrase, GET_CHAR(args, "KeyPassphrase"), MOCA_MAX_PASSWORD_LEN);
        SAH_TRACEZ_INFO(ME, "Set keyphrase=%s", moca_if->keyphrase);
    } else if(GET_ARG(args, "TxPowerLimit")) {
        moca_if->tx_power_limit = GET_UINT32(args, "TxPowerLimit");
        SAH_TRACEZ_INFO(ME, "Set tx_power_limit=%d", moca_if->tx_power_limit);
    } else if(GET_ARG(args, "PowerCntlPhyTarget")) {
        moca_if->power_cntl_phy_target = GET_UINT32(args, "PowerCntlPhyTarget");
        SAH_TRACEZ_INFO(ME, "Set power_cntl_phy_target=%d", moca_if->power_cntl_phy_target);
    } else if(GET_ARG(args, "BeaconPowerLimit")) { // DEPRECATED in 2.17
        moca_if->beacon_power_limit = GET_UINT32(args, "BeaconPowerLimit");
        SAH_TRACEZ_INFO(ME, "Set beacon_power_limit=%d", moca_if->beacon_power_limit);
    } else if(GET_ARG(args, "NodeTabooMask")) {
        strncpy(moca_if->node_taboo_mask, GET_CHAR(args, "NodeTabooMask"), 16);
        SAH_TRACEZ_INFO(ME, "Set node_taboo_mask=%s", moca_if->node_taboo_mask);
    }

    retval = 0;

exit:
    return retval;
}

int dummy_cfg_info_retrieve(UNUSED const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    int retval = -1;
    const char* if_name = NULL;
    dummy_moca_iface_t* moca_if = NULL;

    when_null(ret, exit);
    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");

    if_name = GET_CHAR(args, "InternalName");
    when_null_trace(if_name, exit, ERROR, "Passed interface name is NULL");

    moca_if = find_dummy_moca_iface_by_name(if_name);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    SAH_TRACEZ_INFO(ME, "get cfg data for %s", moca_if->name);

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, ret, "Enable", moca_if->enable);
    amxc_var_add_key(bool, ret, "PreferredNC", moca_if->preferred_nc);
    amxc_var_add_key(bool, ret, "PrivacyEnabledSetting", moca_if->privacy_enabled);
    amxc_var_add_key(uint32_t, ret, "TpcTargetRateNper", moca_if->tpc_phy_rate_nper);
    amxc_var_add_key(cstring_t, ret, "FreqCurrentMaskSetting", moca_if->freq_cur_mask);
    amxc_var_add_key(cstring_t, ret, "KeyPassphrase", moca_if->keyphrase);
    amxc_var_add_key(uint32_t, ret, "TxPowerLimit", moca_if->tx_power_limit);
    amxc_var_add_key(uint32_t, ret, "PowerCntlPhyTarget", moca_if->power_cntl_phy_target);
    // BeaconPowerLimit was DEPRECATED in 2.17
    amxc_var_add_key(uint32_t, ret, "BeaconPowerLimit", moca_if->beacon_power_limit);
    amxc_var_add_key(cstring_t, ret, "NodeTabooMask", moca_if->node_taboo_mask);
    retval = 0;

exit:
    return retval;
}

int dummy_static_info_retrieve(UNUSED const char* function_name,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    int retval = -1;
    const char* if_name = NULL;
    dummy_moca_iface_t* moca_if = NULL;

    when_null(ret, exit);
    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");

    if_name = GET_CHAR(args, "InternalName");
    when_null_trace(if_name, exit, ERROR, "Passed interface name is NULL");

    moca_if = find_dummy_moca_iface_by_name(if_name);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    SAH_TRACEZ_INFO(ME, "Get static data for %s", moca_if->name);

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "InternalName", moca_if->name);
    amxc_var_add_key(cstring_t, ret, "MACAddress", moca_if->mac_addr);
    amxc_var_add_key(cstring_t, ret, "FirmwareVersion", moca_if->firmware_version);
    amxc_var_add_key(cstring_t, ret, "SupportedBands", moca_if->supported_bands);
    amxc_var_add_key(uint32_t, ret, "MaxBitRate", moca_if->maxbitrate);
    amxc_var_add_key(uint32_t, ret, "AggregationSize", moca_if->aggr_size);
    amxc_var_add_key(uint32_t, ret, "AeNumber", moca_if->max_ae_num);
    amxc_var_add_key(uint32_t, ret, "SupportedIngressPqosFlows", moca_if->max_ingress_pqos_flows);
    amxc_var_add_key(uint32_t, ret, "SupportedEgressPqosFlows", moca_if->max_egress_pqos_flows);
    amxc_var_add_key(cstring_t, ret, "HighestVersion", moca_if->highest_version);
    amxc_var_add_key(cstring_t, ret, "FreqCapabilityMask", moca_if->freq_cap_mask);
    amxc_var_add_key(cstring_t, ret, "NetworkTabooMask", moca_if->net_taboo_mask);
    amxc_var_add_key(uint32_t, ret, "TxBcastPowerReduction", moca_if->tx_bcast_power_reduction);
    amxc_var_add_key(bool, ret, "QAM256Capable", moca_if->qam256_capable);
    amxc_var_add_key(bool, ret, "AvbSupport", moca_if->avb_capable);
    amxc_var_add_key(uint32_t, ret, "PacketAggregationCapability", moca_if->packet_aggr_capability);
    retval = 0;

exit:
    return retval;
}

int dummy_dyn_info_retrieve(UNUSED const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    int retval = -1;
    const char* if_name = NULL;
    dummy_moca_iface_t* moca_if = NULL;

    when_null(ret, exit);
    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");

    if_name = GET_CHAR(args, "InternalName");
    when_null_trace(if_name, exit, ERROR, "Passed interface name is NULL");

    moca_if = find_dummy_moca_iface_by_name(if_name);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    SAH_TRACEZ_INFO(ME, "get dyn data for %s", moca_if->name);

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "Status", moca_if->status);
    amxc_var_add_key(cstring_t, ret, "PasswordHash", moca_if->pass_hash);
    amxc_var_add_key(cstring_t, ret, "PowerStateCap", moca_if->power_state_capability);
    amxc_var_add_key(uint32_t, ret, "LinkUpTime", moca_if->linkup_time);
    amxc_var_add_key(uint32_t, ret, "MaxIngressBW", moca_if->max_ingress_bw);
    amxc_var_add_key(uint32_t, ret, "MaxEgressBW", moca_if->max_egress_bw);
    amxc_var_add_key(cstring_t, ret, "CurrentVersion", moca_if->current_version);
    amxc_var_add_key(uint32_t, ret, "NetworkCoordinator", moca_if->network_coord);
    amxc_var_add_key(uint32_t, ret, "NodeID", moca_if->node_id);
    amxc_var_add_key(uint32_t, ret, "BackupNC", moca_if->backup_nc);
    amxc_var_add_key(uint32_t, ret, "NumNodes", moca_if->num_nodes);
    // PrivacyEnabled was DEPRECATED in 2.17
    amxc_var_add_key(bool, ret, "PrivacyEnabled", moca_if->privacy_enabled);
    amxc_var_add_key(cstring_t, ret, "FreqCurrentMask", moca_if->freq_cur_mask);
    amxc_var_add_key(uint32_t, ret, "CurrentOperFreq", moca_if->cur_oper_freq);
    amxc_var_add_key(uint32_t, ret, "LastOperFreq", moca_if->last_oper_freq);
    amxc_var_add_key(uint32_t, ret, "TxBcastRate", moca_if->tx_bcast_rate);
    amxc_var_add_key(uint32_t, ret, "ResetCount", moca_if->reset_count);
    amxc_var_add_key(uint32_t, ret, "LinkDownCount", moca_if->link_down_count);
    amxc_var_add_key(uint32_t, ret, "LmoNodeID", moca_if->lmo_node_id);
    amxc_var_add_key(cstring_t, ret, "NetworkState", moca_if->network_state);
    amxc_var_add_key(int32_t, ret, "PrimaryChannelOffset", moca_if->primary_chan_offset);
    amxc_var_add_key(int32_t, ret, "SecondaryChannelOffset", moca_if->secondary_chan_offset);
    amxc_var_add_key(cstring_t, ret, "ResetReason", moca_if->reset_reason);
    amxc_var_add_key(cstring_t, ret, "NcVersion", moca_if->nc_version);
    amxc_var_add_key(cstring_t, ret, "LinkState", moca_if->link_state);
    retval = 0;

exit:
    return retval;
}

int dummy_qos_info_retrieve(UNUSED const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    int rc = -1;
    const char* ifname = NULL;
    dummy_moca_iface_t* moca_if = NULL;

    when_null(ret, exit);
    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");

    ifname = GET_CHAR(args, "InternalName");
    when_null_trace(ifname, exit, ERROR, "Passed interface name is NULL");

    moca_if = find_dummy_moca_iface_by_name(ifname);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    SAH_TRACEZ_INFO(ME, "get PQoS flows number for %s", moca_if->name);

    rc = 0;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ret, "EgressNumFlows", moca_if->egress_num_flows);
    amxc_var_add_key(uint32_t, ret, "IngressNumFlows", moca_if->ingress_num_flows);
exit:
    return rc;
}

int dummy_flow_info_retrieve(UNUSED const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    int rc = -1;
    const char* ifname = NULL;
    uint32_t flowId;
    dummy_moca_iface_t* moca_if = NULL;
    dummy_moca_flow_t* moca_flow = NULL;

    when_null(ret, exit);
    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");

    ifname = GET_CHAR(args, "InternalName");
    when_null_trace(ifname, exit, ERROR, "Passed interface name is NULL");

    flowId = GET_UINT32(args, "Id");
    when_failed_trace((flowId == 0), exit, ERROR, "Passed flowId is out of range");

    flowId--;

    moca_if = find_dummy_moca_iface_by_name(ifname);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    moca_flow = &moca_if->flows[flowId];

    SAH_TRACEZ_INFO(ME, "get flowId for id %d data for %s", flowId, moca_if->name);

    rc = 0;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "FlowID", moca_flow->flow_id);
    amxc_var_add_key(cstring_t, ret, "PacketDA", moca_flow->packet_da);
    amxc_var_add_key(uint32_t, ret, "MaxRate", moca_flow->max_rate);
    amxc_var_add_key(uint32_t, ret, "MaxBurstSize", moca_flow->max_burst_size);
    amxc_var_add_key(uint32_t, ret, "LeaseTime", moca_flow->lease_time);
    amxc_var_add_key(uint32_t, ret, "Tag", moca_flow->tag);
    amxc_var_add_key(uint32_t, ret, "LeaseTimeLeft", moca_flow->lease_time_left);
    amxc_var_add_key(uint32_t, ret, "FlowPackets", moca_flow->flow_packets);
    amxc_var_add_key(cstring_t, ret, "IngressGuid", moca_flow->ingress_guid);
    amxc_var_add_key(cstring_t, ret, "EgressGuid", moca_flow->egress_guid);
    amxc_var_add_key(uint32_t, ret, "MaximumLatency", moca_flow->max_latency);
    amxc_var_add_key(uint32_t, ret, "ShortTermAvgRatio", moca_flow->short_term_avg_ratio);
    amxc_var_add_key(uint32_t, ret, "FlowPer", moca_flow->flow_per);
    amxc_var_add_key(cstring_t, ret, "IngressClassify", moca_flow->ingress_classify);
    amxc_var_add_key(uint32_t, ret, "VlanTag", moca_flow->vlan_tag);
    amxc_var_add_key(uint32_t, ret, "DscpMoca", moca_flow->dscp);
    amxc_var_add_key(uint32_t, ret, "Dfid", moca_flow->dfid);
exit:
    return rc;
}

int dummy_rlapm_entries_number(UNUSED const char* function_name,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    int rc = -1;
    const char* ifname = NULL;
    dummy_moca_iface_t* moca_if = NULL;

    when_null(ret, exit);
    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");

    ifname = GET_CHAR(args, "InternalName");
    when_null_trace(ifname, exit, ERROR, "Passed interface name is NULL");

    moca_if = find_dummy_moca_iface_by_name(ifname);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    rc = 0;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ret, "RlapmNumberOfEntries", moca_if->rlapm_num);
exit:
    return rc;
}

int dummy_rlapm_info_retrieve(UNUSED const char* function_name,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    int rc = -1;
    const char* ifname = NULL;
    uint32_t entryId;
    dummy_moca_iface_t* moca_if = NULL;
    dummy_moca_rlapm_entry_t* moca_rlapm_entry = NULL;

    when_null(ret, exit);
    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");

    ifname = GET_CHAR(args, "InternalName");
    when_null_trace(ifname, exit, ERROR, "Passed interface name is NULL");

    entryId = GET_UINT32(args, "Id");
    when_failed_trace((entryId == 0), exit, ERROR, "Passed zero entryIde");

    entryId--;

    moca_if = find_dummy_moca_iface_by_name(ifname);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    moca_rlapm_entry = &moca_if->rlapm[entryId];

    SAH_TRACEZ_INFO(ME, "get rlapm for id %d data for %s", entryId, moca_if->name);

    rc = 0;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, ret, "Enable", moca_rlapm_entry->enable);
    amxc_var_add_key(uint32_t, ret, "Profile", moca_rlapm_entry->profile);
    amxc_var_add_key(uint32_t, ret, "Frequency", moca_rlapm_entry->freq);
    amxc_var_add_key(uint32_t, ret, "GlobalAggrRxPwrLevel", moca_rlapm_entry->garpl);
    amxc_var_add_key(uint32_t, ret, "PhyMargin", moca_rlapm_entry->phy_margin);
    amxc_var_add_key(cstring_t, ret, "Status", moca_rlapm_entry->status);
exit:
    return rc;
}

int dummy_rlapm_info_set(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    int retval = -1;
    const char* if_name = NULL;
    uint32_t entryId;
    dummy_moca_iface_t* moca_if = NULL;
    dummy_moca_rlapm_entry_t* moca_rlapm_entry = NULL;

    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");
    if_name = GET_CHAR(args, "InternalName");
    when_null_trace(if_name, exit, ERROR, "Passed interface name is NULL");

    entryId = GET_UINT32(args, "Id");
    when_failed_trace((entryId == 0), exit, ERROR, "Passed zero entryId");

    entryId--;

    moca_if = find_dummy_moca_iface_by_name(if_name);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    moca_rlapm_entry = &moca_if->rlapm[entryId];

    SAH_TRACEZ_INFO(ME, "set rlapm for id %d data for %s", entryId, moca_if->name);

    SAH_TRACEZ_INFO(ME, "Setting for interface: %s", moca_if->name);

    if(GET_ARG(args, "Enable")) {
        moca_rlapm_entry->enable = GET_BOOL(args, "Enable");
        if(moca_rlapm_entry->enable) {
            strcpy(moca_rlapm_entry->status, "active");
        } else {
            strcpy(moca_rlapm_entry->status, "notInService");
        }
        SAH_TRACEZ_INFO(ME, "Set enable=%d", moca_rlapm_entry->enable);
    } else if(GET_ARG(args, "Profile")) {
        moca_rlapm_entry->profile = GET_UINT32(args, "Profile");
        SAH_TRACEZ_INFO(ME, "Set profile=%d", moca_rlapm_entry->profile);
    } else if(GET_ARG(args, "Frequency")) {
        moca_rlapm_entry->freq = GET_UINT32(args, "Frequency");
        SAH_TRACEZ_INFO(ME, "Set freq=%d", moca_rlapm_entry->freq);
    } else if(GET_ARG(args, "GlobalAggrRxPwrLevel")) {
        moca_rlapm_entry->garpl = GET_UINT32(args, "GlobalAggrRxPwrLevel");
        SAH_TRACEZ_INFO(ME, "Set garpl=%d", moca_rlapm_entry->garpl);
    } else if(GET_ARG(args, "PhyMargin")) {
        moca_rlapm_entry->phy_margin = GET_UINT32(args, "PhyMargin");
        SAH_TRACEZ_INFO(ME, "Set phy_margin=%d", moca_rlapm_entry->phy_margin);
    }

    retval = 0;

exit:
    return retval;
}

int dummy_sapm_entries_number(UNUSED const char* function_name,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    int rc = -1;
    const char* ifname = NULL;
    dummy_moca_iface_t* moca_if = NULL;

    when_null(ret, exit);
    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");

    ifname = GET_CHAR(args, "InternalName");
    when_null_trace(ifname, exit, ERROR, "Passed interface name is NULL");

    moca_if = find_dummy_moca_iface_by_name(ifname);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    rc = 0;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ret, "SapmNumberOfEntries", moca_if->sapm_num);
exit:
    return rc;
}

int dummy_sapm_info_retrieve(UNUSED const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    int rc = -1;
    const char* ifname = NULL;
    uint32_t entryId;
    dummy_moca_iface_t* moca_if = NULL;
    dummy_moca_sapm_entry_t* moca_sapm_entry = NULL;

    when_null(ret, exit);
    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");

    ifname = GET_CHAR(args, "InternalName");
    when_null_trace(ifname, exit, ERROR, "Passed interface name is NULL");

    entryId = GET_UINT32(args, "Id");
    when_failed_trace((entryId == 0), exit, ERROR, "Passed zero entryIde");

    entryId--;

    moca_if = find_dummy_moca_iface_by_name(ifname);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    moca_sapm_entry = &moca_if->sapm[entryId];

    SAH_TRACEZ_INFO(ME, "get sapm for id %d data for %s", entryId, moca_if->name);

    rc = 0;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, ret, "Enable", moca_sapm_entry->enable);
    amxc_var_add_key(uint32_t, ret, "Profile", moca_sapm_entry->profile);
    amxc_var_add_key(uint32_t, ret, "Frequency", moca_sapm_entry->freq);
    amxc_var_add_key(uint32_t, ret, "AggrRxPwrLevelThreshold", moca_sapm_entry->aggr_rx_pwr_level_threshold);
    amxc_var_add_key(cstring_t, ret, "PhyMargin", moca_sapm_entry->phy_margin);
    amxc_var_add_key(cstring_t, ret, "Status", moca_sapm_entry->status);
exit:
    return rc;
}

int dummy_sapm_info_set(UNUSED const char* function_name,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    int retval = -1;
    const char* if_name = NULL;
    uint32_t entryId;
    dummy_moca_iface_t* moca_if = NULL;
    dummy_moca_sapm_entry_t* moca_sapm_entry = NULL;

    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");
    if_name = GET_CHAR(args, "InternalName");
    when_null_trace(if_name, exit, ERROR, "Passed interface name is NULL");

    entryId = GET_UINT32(args, "Id");
    when_failed_trace((entryId == 0), exit, ERROR, "Passed zero entryId");

    entryId--;

    moca_if = find_dummy_moca_iface_by_name(if_name);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    moca_sapm_entry = &moca_if->sapm[entryId];

    SAH_TRACEZ_INFO(ME, "set sapm for id %d data for %s", entryId, moca_if->name);

    SAH_TRACEZ_INFO(ME, "Setting for interface: %s", moca_if->name);

    if(GET_ARG(args, "Enable")) {
        moca_sapm_entry->enable = GET_BOOL(args, "Enable");
        if(moca_sapm_entry->enable) {
            strcpy(moca_sapm_entry->status, "active");
        } else {
            strcpy(moca_sapm_entry->status, "notInService");
        }
        SAH_TRACEZ_INFO(ME, "Set enable=%d", moca_sapm_entry->enable);
    } else if(GET_ARG(args, "Profile")) {
        moca_sapm_entry->profile = GET_UINT32(args, "Profile");
        SAH_TRACEZ_INFO(ME, "Set profile=%d", moca_sapm_entry->profile);
    } else if(GET_ARG(args, "Frequency")) {
        moca_sapm_entry->freq = GET_UINT32(args, "Frequency");
        SAH_TRACEZ_INFO(ME, "Set freq=%d", moca_sapm_entry->freq);
    } else if(GET_ARG(args, "AggrRxPwrLevelThreshold")) {
        moca_sapm_entry->aggr_rx_pwr_level_threshold = GET_UINT32(args, "AggrRxPwrLevelThreshold");
        SAH_TRACEZ_INFO(ME, "Set aggr_rx_pwr_level_threshold=%d", moca_sapm_entry->aggr_rx_pwr_level_threshold);
    } else if(GET_ARG(args, "PhyMargin")) {
        strncpy(moca_sapm_entry->phy_margin, GET_CHAR(args, "PhyMargin"), 255);
        SAH_TRACEZ_INFO(ME, "Set phy_margin=%s", moca_sapm_entry->phy_margin);
    }

    retval = 0;

exit:
    return retval;
}

int dummy_node_info_retrieve(UNUSED const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    int retval = -1;
    const char* if_name = NULL;
    uint32_t nodeId;
    dummy_moca_iface_t* moca_if = NULL;
    dummy_moca_node_t* moca_node = NULL;

    when_null(ret, exit);
    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");

    if_name = GET_CHAR(args, "InternalName");
    when_null_trace(if_name, exit, ERROR, "Passed interface name is NULL");

    nodeId = GET_UINT32(args, "Id");
    when_failed_trace(((nodeId > MOCA_MAX_NODES) || (nodeId == 0)), exit, ERROR, "Passed nodeid is out of range");

    nodeId--;

    moca_if = find_dummy_moca_iface_by_name(if_name);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    moca_node = &moca_if->nodes[nodeId];

    SAH_TRACEZ_INFO(ME, "get node id %d data for %s", nodeId, moca_if->name);

    if(nodeId == moca_if->node_id) {
        // Entry for local node will be exposed as empty
        memset(moca_node, 0, sizeof(dummy_moca_node_t));
    }

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "MACAddress", moca_node->mac_addr);
    amxc_var_add_key(uint32_t, ret, "NodeID", moca_node->node_id);
    amxc_var_add_key(bool, ret, "PreferredNC", moca_node->preferred_nc);
    amxc_var_add_key(cstring_t, ret, "HighestVersion", moca_node->highest_version);
    amxc_var_add_key(uint32_t, ret, "PHYTxRate", moca_node->phy_tx_rate);
    amxc_var_add_key(uint32_t, ret, "PHYRxRate", moca_node->phy_rx_rate);
    // TxPowerControlReduction and RxPowerLevel were DEPRECATED in 2.17
    amxc_var_add_key(uint32_t, ret, "TxPowerControlReduction", moca_node->tx_power_control_reduction);
    amxc_var_add_key(uint32_t, ret, "RxPowerLevel", moca_node->rx_power_level);
    amxc_var_add_key(uint32_t, ret, "TxBcastRate", moca_node->tx_bcast_rate);
    amxc_var_add_key(int32_t, ret, "RxBcastPowerLevel", moca_node->rx_bcast_power_level);
    amxc_var_add_key(uint64_t, ret, "TxPackets", moca_node->tx_packets);
    amxc_var_add_key(uint32_t, ret, "TxDrops", moca_node->tx_drops);
    amxc_var_add_key(uint64_t, ret, "RxPackets", moca_node->rx_packets);
    amxc_var_add_key(uint64_t, ret, "RxCorrected", moca_node->rx_corrected);
    amxc_var_add_key(uint32_t, ret, "RxErroredAndMissedPackets", moca_node->rx_errored_and_missed_packets);
    amxc_var_add_key(bool, ret, "BondingCapable", moca_node->bonding_capable);
    amxc_var_add_key(bool, ret, "QAM256Capable", moca_node->qam256_capable);
    amxc_var_add_key(uint32_t, ret, "PacketAggregationCapability", moca_node->packet_aggr_capability);
    // RxSNR was DEPRECATED in 2.17
    amxc_var_add_key(uint32_t, ret, "RxSNR", moca_node->rx_snr);
    amxc_var_add_key(bool, ret, "Active", moca_node->active);
    amxc_var_add_key(uint32_t, ret, "SupportedIngressPqosFlows", moca_node->max_ingress_pqos_flows);
    amxc_var_add_key(uint32_t, ret, "SupportedEgressPqosFlows", moca_node->max_egress_pqos_flows);
    amxc_var_add_key(uint32_t, ret, "AggregationSize", moca_node->aggr_size);
    amxc_var_add_key(uint32_t, ret, "AeNumber", moca_node->max_ae_num);
    amxc_var_add_key(cstring_t, ret, "PowerState", moca_node->power_state);
    amxc_var_add_key(cstring_t, ret, "PowerStateCapability", moca_node->power_state_capability);
    amxc_var_add_key(bool, ret, "Moca25PhyCapable", moca_node->moca25_phy_capable);
    retval = 0;

exit:
    return retval;
}


int dummy_mesh_info_retrieve(UNUSED const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    int rc = -1;
    const char* ifname = NULL;
    uint32_t entryId;
    dummy_moca_iface_t* moca_if = NULL;
    dummy_moca_mesh_entry_t* moca_mesh_entry = NULL;
    uint32_t tx_node_id;
    uint32_t rx_node_id;

    when_null(ret, exit);
    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");

    ifname = GET_CHAR(args, "InternalName");
    when_null_trace(ifname, exit, ERROR, "Passed interface name is NULL");

    entryId = GET_UINT32(args, "Id");
    when_failed_trace(((entryId > MOCA_MAX_NODES * MOCA_MAX_NODES) || (entryId == 0)), exit, ERROR, "Passed entryId is out of range");

    entryId--;

    moca_if = find_dummy_moca_iface_by_name(ifname);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    tx_node_id = entryId / MOCA_MAX_NODES;
    rx_node_id = entryId % MOCA_MAX_NODES;

    moca_mesh_entry = &moca_if->meshes[tx_node_id].entry[rx_node_id];

    SAH_TRACEZ_INFO(ME, "get mesh for id %d data for %s", entryId, moca_if->name);

    rc = 0;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ret, "TxNodeIndex", tx_node_id);
    amxc_var_add_key(uint32_t, ret, "RxNodeIndex", rx_node_id);
    amxc_var_add_key(uint32_t, ret, "TxRate", moca_mesh_entry->tx_rate);
    amxc_var_add_key(uint32_t, ret, "TxRateNper", moca_mesh_entry->tx_rate_nper);
    amxc_var_add_key(uint32_t, ret, "TxRateVlper", moca_mesh_entry->tx_rate_vlper);
exit:
    return rc;
}

int dummy_bridge_info_retrieve(UNUSED const char* function_name,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    int rc = -1;
    const char* ifname = NULL;
    uint32_t nodeId;
    dummy_moca_iface_t* moca_if = NULL;
    dummy_moca_bridge_t* moca_bridge = NULL;

    when_null(ret, exit);
    when_null_trace(args, exit, ERROR, "Passed args is NULL");
    when_failed_trace((amxc_var_type_of(args) != AMXC_VAR_ID_HTABLE), exit, ERROR,
                      "args is not an htable");

    ifname = GET_CHAR(args, "InternalName");
    when_null_trace(ifname, exit, ERROR, "Passed interface name is NULL");

    nodeId = GET_UINT32(args, "Id");
    when_failed_trace(((nodeId > MOCA_MAX_NODES) || (nodeId == 0)), exit, ERROR, "Passed nodeid is out of range");

    nodeId--;

    moca_if = find_dummy_moca_iface_by_name(ifname);
    when_null_trace(moca_if, exit, ERROR, "Unable to find a dummy iface by name");

    moca_bridge = &moca_if->bridges[nodeId];

    SAH_TRACEZ_INFO(ME, "get bridge for node id %d data for %s", nodeId, moca_if->name);

    rc = 0;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ret, "NodeIndex", moca_bridge->node_id);
    amxc_var_add_key(csv_string_t, ret, "MACAddresses", moca_bridge->mac_addresses);
exit:
    return rc;
}

static AMXM_CONSTRUCTOR dummy_module_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxc_llist_new(&dummy_moca_ifaces);

    amxm_module_register(&mod, so, MOD_MOCA_CTRL);
    amxm_module_add_function(mod, "create-moca-iface", dummy_iface_create);
    amxm_module_add_function(mod, "destroy-moca-iface", dummy_iface_destroy);
    amxm_module_add_function(mod, "set-cfg-info", dummy_cfg_info_set);
    amxm_module_add_function(mod, "retrieve-cfg-info", dummy_cfg_info_retrieve);
    amxm_module_add_function(mod, "retrieve-static-info", dummy_static_info_retrieve);
    amxm_module_add_function(mod, "retrieve-dyn-info", dummy_dyn_info_retrieve);
    amxm_module_add_function(mod, "retrieve-qos-info", dummy_qos_info_retrieve);
    amxm_module_add_function(mod, "retrieve-flow-info", dummy_flow_info_retrieve);
    amxm_module_add_function(mod, "retrieve-rlapm-number", dummy_rlapm_entries_number);
    amxm_module_add_function(mod, "set-rlapm-info", dummy_rlapm_info_set);
    amxm_module_add_function(mod, "retrieve-rlapm-info", dummy_rlapm_info_retrieve);
    amxm_module_add_function(mod, "retrieve-sapm-number", dummy_sapm_entries_number);
    amxm_module_add_function(mod, "set-sapm-info", dummy_sapm_info_set);
    amxm_module_add_function(mod, "retrieve-sapm-info", dummy_sapm_info_retrieve);
    amxm_module_add_function(mod, "retrieve-node-info", dummy_node_info_retrieve);
    amxm_module_add_function(mod, "retrieve-mesh-info", dummy_mesh_info_retrieve);
    amxm_module_add_function(mod, "retrieve-bridge-info", dummy_bridge_info_retrieve);

    SAH_TRACEZ_NOTICE(ME, "Module loaded");

    return 0;
}

static AMXM_DESTRUCTOR dummy_module_stop(void) {

    SAH_TRACEZ_NOTICE(ME, "Module stopped");

    amxc_llist_delete(&dummy_moca_ifaces, dummy_iface_delete);
    return 0;
}
