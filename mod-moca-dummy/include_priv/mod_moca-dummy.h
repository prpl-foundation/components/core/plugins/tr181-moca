/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SMOCA_HALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_MOCA_DUMMY_H__)
#define __MOD_MOCA_DUMMY_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxm/amxm.h>

#include <moca_priv.h>

#define MOCA_MAX_PASSWORD_LEN 17
#define MOCA_MAX_RLAPM_ENTRIES 32
#define MOCA_MAX_SAPM_ENTRIES 4
#define MOCA_MAX_SAPM_SUBCARRIER_ENTRIES 64

// module names
#define MOD_MOCA_CTRL "moca-ctrl"

typedef struct _moca_rlapm_entry {
    bool enable;
    uint32_t profile;
    uint32_t freq;
    uint32_t garpl;
    uint32_t phy_margin;
    char status[16];
} dummy_moca_rlapm_entry_t;

typedef struct _moca_sapm_entry {
    bool enable;
    uint32_t profile;
    uint32_t freq;
    uint32_t aggr_rx_pwr_level_threshold;
    char phy_margin[256];
    char status[16];
} dummy_moca_sapm_entry_t;

typedef struct _moca_flow {
    char flow_id[18];
    char packet_da[18];
    uint32_t max_rate;
    uint32_t max_burst_size;
    uint32_t lease_time;
    uint32_t tag;
    uint32_t lease_time_left;
    uint32_t flow_packets;
    char ingress_guid[18];
    char egress_guid[18];
    uint32_t max_latency;
    uint32_t short_term_avg_ratio;
    uint32_t flow_per;
    char ingress_classify[64];
    uint32_t vlan_tag;
    uint32_t dscp;
    uint32_t dfid;
} dummy_moca_flow_t;

typedef struct _moca_node {
    char mac_addr[18];
    uint32_t node_id;
    bool preferred_nc;
    char highest_version[16];
    uint32_t phy_tx_rate;
    uint32_t phy_rx_rate;
    uint32_t tx_power_control_reduction; // DEPRECATED in 2.17
    uint32_t rx_power_level;             // DEPRECATED in 2.17
    uint32_t tx_bcast_rate;
    int32_t rx_bcast_power_level;
    uint64_t tx_packets;
    uint32_t tx_drops;
    uint64_t rx_packets;
    uint64_t rx_corrected;
    uint32_t rx_errored_and_missed_packets;
    bool bonding_capable;
    bool qam256_capable;
    uint32_t packet_aggr_capability;
    uint32_t rx_snr; // DEPRECATED in 2.17
    uint32_t max_ingress_pqos_flows;
    uint32_t max_egress_pqos_flows;
    uint32_t aggr_size;
    uint32_t max_ae_num;
    char power_state[16];
    char power_state_capability[16];
    bool active;
    bool moca25_phy_capable;
} dummy_moca_node_t;

typedef struct _moca_mesh_entry {
    uint32_t tx_rate;
    uint32_t tx_rate_nper;
    uint32_t tx_rate_vlper;
} dummy_moca_mesh_entry_t;

typedef struct _moca_mesh {
    dummy_moca_mesh_entry_t entry[MOCA_MAX_NODES];
} dummy_moca_mesh_t;

typedef struct _moca_bridge {
    uint32_t node_id;
    char mac_addresses[18 * MOCA_MAX_BRIDGE_ENTRIES_PER_NODE];
} dummy_moca_bridge_t;

typedef struct _moca_dummy_iface {
    /* Read-only configuration parameters */
    char name[16];        // Equals to DM Parameter InternalName
    char netdev_name[16]; // Equals to DM Parameter Name
    char mac_addr[18];
    char firmware_version[64];
    char supported_bands[128];
    uint32_t maxbitrate;
    uint32_t aggr_size;
    uint32_t max_ae_num;
    uint32_t max_ingress_pqos_flows;
    uint32_t max_egress_pqos_flows;
    char highest_version[16];
    char freq_cap_mask[17];
    char net_taboo_mask[17];
    uint32_t tx_bcast_power_reduction;
    bool qam256_capable;
    bool avb_capable;
    uint32_t packet_aggr_capability;
    /* Writable configuration parameters */
    bool enable;
    bool preferred_nc;
    bool privacy_enabled;
    uint32_t tpc_phy_rate_nper;
    char freq_cur_mask[17];
    char keyphrase[MOCA_MAX_PASSWORD_LEN + 1];
    uint32_t tx_power_limit;
    uint32_t power_cntl_phy_target;
    int32_t beacon_power_limit; // DEPRECATED in 2.17
    char node_taboo_mask[17];
    uint32_t rlapm_num;
    dummy_moca_rlapm_entry_t rlapm[MOCA_MAX_RLAPM_ENTRIES];
    uint32_t sapm_num;
    dummy_moca_sapm_entry_t sapm[MOCA_MAX_SAPM_ENTRIES];
    /* Dynamic parameters */
    char status[8];
    char pass_hash[5];
    char power_state_capability[16];
    uint32_t linkup_time;
    uint32_t max_ingress_bw;
    uint32_t max_egress_bw;
    char current_version[16];
    uint32_t network_coord;
    uint32_t node_id;
    uint32_t backup_nc;
    uint32_t num_nodes;
    uint32_t cur_oper_freq;
    uint32_t last_oper_freq;
    uint32_t tx_bcast_rate;
    uint32_t reset_count;
    uint32_t link_down_count;
    uint32_t lmo_node_id;
    char network_state[32];
    int32_t primary_chan_offset;
    int32_t secondary_chan_offset;
    char reset_reason[80];
    char nc_version[64];
    char link_state[7];
    uint32_t ingress_num_flows;
    uint32_t egress_num_flows;
    dummy_moca_flow_t flows[MOCA_MAX_FLOWS];
    dummy_moca_node_t nodes[MOCA_MAX_NODES];
    dummy_moca_mesh_t meshes[MOCA_MAX_NODES];
    dummy_moca_bridge_t bridges[MOCA_MAX_NODES];
    amxc_llist_it_t it;
} dummy_moca_iface_t;

int dummy_iface_create(const char* function_name,
                       amxc_var_t* args,
                       amxc_var_t* ret);

int dummy_iface_destroy(const char* function_name,
                        amxc_var_t* args,
                        amxc_var_t* ret);

int dummy_cfg_info_set(const char* function_name,
                       amxc_var_t* args,
                       amxc_var_t* ret);

int dummy_cfg_info_retrieve(const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret);

int dummy_static_info_retrieve(const char* function_name,
                               amxc_var_t* args,
                               amxc_var_t* ret);

int dummy_dyn_info_retrieve(const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret);

int dummy_qos_info_retrieve(const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret);

int dummy_flow_info_retrieve(const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret);

int dummy_rlapm_entries_number(const char* function_name,
                               amxc_var_t* args,
                               amxc_var_t* ret);

int dummy_rlapm_info_set(const char* function_name,
                         amxc_var_t* args,
                         amxc_var_t* ret);

int dummy_rlapm_info_retrieve(const char* function_name,
                              amxc_var_t* args,
                              amxc_var_t* ret);

int dummy_sapm_entries_number(const char* function_name,
                              amxc_var_t* args,
                              amxc_var_t* ret);

int dummy_sapm_info_set(const char* function_name,
                        amxc_var_t* args,
                        amxc_var_t* ret);

int dummy_sapm_info_retrieve(const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret);

int dummy_node_info_retrieve(const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret);

int dummy_mesh_info_retrieve(const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret);

int dummy_bridge_info_retrieve(UNUSED const char* function_name,
                               amxc_var_t* args,
                               amxc_var_t* ret);
#ifdef __cplusplus
}
#endif

#endif // __MOD_MOCA_DUMMY_H__
